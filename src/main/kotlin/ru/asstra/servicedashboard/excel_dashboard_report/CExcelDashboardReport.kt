package ru.asstra.servicedashboard.excel_dashboard_report

import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.ss.usermodel.*
import org.apache.poi.xddf.usermodel.chart.*
import org.apache.poi.xssf.usermodel.*
import org.apache.poi.xddf.usermodel.*
import org.apache.poi.xddf.usermodel.chart.ChartTypes
import org.apache.poi.xddf.usermodel.chart.XDDFDataSourcesFactory
import org.apache.poi.xddf.usermodel.chart.XDDFPieChartData
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.stereotype.Service
import ru.asstra.servicedashboard.model.COrganization
import ru.asstra.servicedashboard.rest.IRestTemplate
import ru.asstra.servicedashboard.services.IServiceDashboard
//import ru.swiftgroup.project.api.jpa.dashboard.IServiceDashboard
//import ru.swiftgroup.project.api.jpa.offence_type.IServiceOffenceType
//import ru.swiftgroup.project.api.jpa.offences.IServiceOffence
//import ru.swiftgroup.project.api.jpa.statuses.IStatusService
//import ru.swiftgroup.project.api.jpa.tasks.IServiceTask
//import ru.swiftgroup.project.api.jpa.user_workspace_relation.IUserWorkspaceRelationRepository
//import ru.swiftgroup.project.api.jpa.workspaces.CWorkspaceService
//import ru.swiftgroup.project.api.model.COrganization
import java.awt.Color
import java.util.*
import java.io.File
import java.io.IOException
import java.io.FileOutputStream
import java.time.format.DateTimeFormatter
import java.time.LocalDate
import java.text.DecimalFormat

/********************************************************************************************************
 * Класс содержит методы построения отчёта Excel о статусе проекта.                                     *
 * @author Теплюк О.В. 2019 0726.                                                                       *
 *******************************************************************************************************/
@Service
class CExcelDashboardReport                 : IExcelDashboardReport
{
    @Autowired
    lateinit var dashboardService           : IServiceDashboard
    @Autowired
    lateinit var restTemplate               : IRestTemplate
//    @Autowired
//    lateinit var workspaceService           : CWorkspaceService
//    @Autowired
//    lateinit var uwrRepository              : IUserWorkspaceRelationRepository
//    @Autowired
//    lateinit var statusService              : IStatusService
//    @Autowired
//    lateinit var serviceOffenceType         : IServiceOffenceType
//    @Autowired
//    lateinit var taskService                : IServiceTask
//    @Autowired
//    lateinit var offenceService             : IServiceOffence

    /****************************************************************************************************
     * Объект с переводами текстовых контстант.                                                         *
     ***************************************************************************************************/
    @Autowired
    lateinit var messageSource              : MessageSource

    override fun createReportExcel(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID,
            locale                          : Locale
    )                                       : XSSFWorkbook
    {
        //TODO Перевод текстовых сообщений.
        val wb                              = XSSFWorkbook()
        val styles                          = createStyles(wb)
        val fonts                           = createFonts(wb)

        // Создание вкладки "Легенда проекта"
        val sheet1                           = wb.createSheet("Легенда проекта")
//        createProjectLegend(authentication, workspaceId, sheet1, styles, locale)
        // Создание вкладки "Документы"
        val sheet2                           = wb.createSheet("Документы")
//        createDocuments(authentication, dateFrom, dateTo, workspaceId, sheet2, styles, locale)
        // Создание вкладки "Пользователи"
        val sheet3                           = wb.createSheet("Пользователи")
        createUsers(authentication, dateFrom, dateTo, workspaceId, sheet3, styles, locale)
        // Создание вкладки "Поручения"
        val sheet4                           = wb.createSheet("Поручения")
//        createTasks(dateFrom, dateTo, workspaceId, sheet4, styles, locale)
        // Создание вкладки "Нарушения"
        val sheet5                           = wb.createSheet("Нарушения")
 //       createOffences(authentication, dateFrom, dateTo, workspaceId, sheet5, styles, locale)

        // записываем созданный в памяти Excel документ в файл
        try
        {
            FileOutputStream(File("D:\\Excel_File.xlsx")).use({ out -> wb.write(out) })
        }
        catch (e: IOException)
        {
            e.printStackTrace()
            println("Провал!")
        }

        return wb
    }

    /****************************************************************************************************
     * Создаёт таблицу со стилями ячеек.                                                                *
     * @param wb - книга Excel для вывода списка поручений.                                             *
     ***************************************************************************************************/
    private fun createStyles(
            wb                              : XSSFWorkbook
    )                                       : HashMap<String, XSSFCellStyle>
    {
        val colorMap                        = wb.stylesSource.indexedColors
        val white                           = XSSFColor(Color.WHITE, colorMap)
        val styles                          = HashMap<String, XSSFCellStyle>()

        var font                            = wb.createFont()
        font.fontName                       = "Arial"
        font.bold                           = true
        font.fontHeight                     = 200

        //Стиль заголовка.
        var cellStyle                       = wb.createCellStyle()
        cellStyle.wrapText                  = true
        cellStyle.verticalAlignment         = VerticalAlignment.CENTER
        cellStyle.alignment                 = HorizontalAlignment.CENTER
        cellStyle.borderBottom              = BorderStyle.MEDIUM
        cellStyle.borderLeft                = BorderStyle.MEDIUM
        cellStyle.borderTop                 = BorderStyle.MEDIUM
        cellStyle.borderRight               = BorderStyle.MEDIUM
        cellStyle.setFillForegroundColor(white)
        cellStyle.fillPattern               = FillPatternType.SOLID_FOREGROUND
        cellStyle.setFont(font)
        styles["TopHeader"]                 = cellStyle

        font                                = wb.createFont()
        font.fontName                       = "Arial"
        font.bold                           = true
        font.setColor(XSSFColor(Color.WHITE, colorMap))
        font.fontHeight                     = 200

        //Стиль шапки таблицы.
        cellStyle                           = wb.createCellStyle()
        cellStyle.wrapText                  = true
        cellStyle.verticalAlignment         = VerticalAlignment.BOTTOM
        cellStyle.alignment                 = HorizontalAlignment.CENTER
        cellStyle.borderBottom              = BorderStyle.THIN
        cellStyle.borderLeft                = BorderStyle.THIN
        cellStyle.borderTop                 = BorderStyle.THIN
        cellStyle.borderRight               = BorderStyle.THIN
        cellStyle.setLeftBorderColor(white)
        cellStyle.setRightBorderColor(white)
        cellStyle.setTopBorderColor(white)
        cellStyle.setBottomBorderColor(white)
        cellStyle.setFillForegroundColor(XSSFColor(Color(0, 32, 96), colorMap))
        cellStyle.fillPattern               = FillPatternType.SOLID_FOREGROUND
        cellStyle.setFont(font)

        styles["TableHeader"]               = cellStyle

        font                                = wb.createFont()
        font.fontName                       = "Arial"
        font.fontHeight                     = 200

        //Стиль ячеек основной части таблицы.
        cellStyle                       = wb.createCellStyle()
        cellStyle.wrapText                  = true
        cellStyle.verticalAlignment         = VerticalAlignment.TOP
        cellStyle.alignment                 = HorizontalAlignment.LEFT
        cellStyle.borderBottom              = BorderStyle.THIN
        cellStyle.borderLeft                = BorderStyle.THIN
        cellStyle.borderTop                 = BorderStyle.THIN
        cellStyle.borderRight               = BorderStyle.THIN
        cellStyle.setFillForegroundColor(white)
        cellStyle.fillPattern               = FillPatternType.SOLID_FOREGROUND
        cellStyle.setFont(font)
        styles["TableCommon"]                    = cellStyle

        font                                = wb.createFont()
        font.fontName                       = "Arial"
        font.fontHeight                     = 200

        //Стиль ячеек по-умолчанию.
        cellStyle                           = wb.createCellStyle()
        cellStyle.wrapText                  = true
        cellStyle.verticalAlignment         = VerticalAlignment.TOP
        cellStyle.alignment                 = HorizontalAlignment.LEFT
        cellStyle.setFillForegroundColor(white)
        cellStyle.fillPattern               = FillPatternType.SOLID_FOREGROUND
        cellStyle.setFont(font)
        styles["Default"]                    = cellStyle

        return styles
    }

    private fun createFonts(
            wb                              : XSSFWorkbook
    )                                       : HashMap<String, XSSFFont>
    {
        val fonts                           = HashMap<String, XSSFFont>()
        var font                            = wb.createFont()
        font.italic                         = true
        font.fontName                       = "Arial"
        font.fontHeight                     = 200
        fonts["UserName"]                   = font

        font                                = wb.createFont()
        font.fontName                       = "Arial"
        font.fontHeight                     = 200
        fonts["Message"]                    = font

        return fonts
    }

    /****************************************************************************************************
     * Формирует тело таблицы на переданном листе.                                                      *
     * @param sheet - лист из файла Excel для вывода списка поручений.                                  *
     * @param styles - карта со стилями.                                                                *
     * @param locale - язык отчёта.                                                                     *
     ***************************************************************************************************/
    private fun createProjectLegend(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            sheet                           : XSSFSheet,
            styles                          : HashMap<String, XSSFCellStyle>,
            locale                          : Locale
    )
    {
        val workspace                       = restTemplate.getWorkspace(authentication, workspaceId).get()
        val style                           = styles["TopHeader"]!!

        //Шапка
        var row                             = sheet.createRow(0)
        var cell                            = row.createCell(0)
        cell.setCellValue(messageSource.getMessage("Project legend", null, locale))
        cell.cellStyle                      = style
        row.height                          = 400
        cell                                = row.createCell(1)
        cell.cellStyle                      = style
        sheet.addMergedRegion(CellRangeAddress(0, 0, 0, 1))

        // Основная часть
        row                                 = sheet.createRow(1)
        cell                                = row.createCell(0)
        cell.setCellValue(messageSource.getMessage("Project", null, locale) + ":")
        cell                                = row.createCell(1)
        cell.setCellValue(workspace.name)

        row                                 = sheet.createRow(2)
        cell                                = row.createCell(0)
        cell.setCellValue(messageSource.getMessage("Organization", null, locale) + ":")
        cell                                = row.createCell(1)
        cell.setCellValue(workspace.organization!!.name)

        row                                 = sheet.createRow(3)
        cell                                = row.createCell(0)
        cell.setCellValue("Плановые сроки работ:")
        cell                                = row.createCell(1)
        val formatter                     = DateTimeFormatter.ofPattern("dd.MM.yyyy")
        val begin                           = workspace.dateFromPlan!!.format(formatter)
        val end                             = workspace.dateToPlan!!.format(formatter)
        cell.setCellValue(begin.toString() + " - " + end.toString())

        // Документы
        row                                 = sheet.createRow(5)
        cell                                = row.createCell(0)
        cell.setCellValue("Документы")
        row                                 = sheet.createRow(6)
        cell                                = row.createCell(0)
        cell.setCellValue("Общий вес, Гб" + ":")
        cell                                = row.createCell(1)
        val size                            = dashboardService.documentsTotalSize(authentication, workspaceId)!!
        // Переводим из бит в Гб
        val floatSize                       = size.toFloat() / 8 / 1024 / 1024 / 1024
        val formattedFloat                  = DecimalFormat("#0.000").format(floatSize)
        cell.setCellValue(formattedFloat)
        row                                 = sheet.createRow(7)
        cell                                = row.createCell(0)
        cell.setCellValue("Загружено всего" + ":")
        cell                                = row.createCell(1)
        cell.setCellValue(dashboardService.getDocumentsCount(authentication, workspaceId).toString())

        // Пользователи
        row                                 = sheet.createRow(8)
        row                                 = sheet.createRow(9)
        cell                                = row.createCell(0)
        cell.setCellValue("Пользователи")
        row                                 = sheet.createRow(10)
        cell                                = row.createCell(0)
        cell.setCellValue("Общее число пользователей" + ":")
        cell                                = row.createCell(1)
        val count                           = restTemplate.getFindUserByWorkspaceId(authentication, workspaceId).size
        cell.setCellValue(count.toString())

        // Поручения
        row                                 = sheet.createRow(11)
        row                                 = sheet.createRow(12)
        cell                                = row.createCell(0)
        cell.setCellValue("Поручения")
        row                                 = sheet.createRow(13)
        cell                                = row.createCell(0)
        cell.setCellValue("Общее число поручений" + ":")
        cell                                = row.createCell(1)
        val countTask                       = restTemplate.getTaskByWorkspaceId(authentication, workspaceId).toList().size
        cell.setCellValue(countTask.toString())

        // Нарушения
        row                                 = sheet.createRow(11)
        row                                 = sheet.createRow(12)
        cell                                = row.createCell(0)
        cell.setCellValue("Поручения")
        row                                 = sheet.createRow(13)
        cell                                = row.createCell(0)
        cell.setCellValue("Общее число поручений" + ":")
        cell                                = row.createCell(1)
        val countOffence                    = restTemplate.getOffencesByWorkspaceId(authentication, workspaceId).toList().size
        cell.setCellValue(countOffence.toString())

        sheet.setColumnWidth(0, 7000)
        sheet.setColumnWidth(1, 8000)
    }

    private fun createDocuments(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID,
            sheet                           : XSSFSheet,
            styles                          : HashMap<String, XSSFCellStyle>,
            locale                          : Locale
    )
    {
        val style                           = styles["TopHeader"]!!

        //Шапка
        var row                             = sheet.createRow(0)
        var cell                            = row.createCell(0)
        cell.setCellValue(messageSource.getMessage("Documents", null, locale))
        cell.cellStyle                      = style
        row.height                          = 400
        cell                                = row.createCell(1)
        cell.cellStyle                      = style
        sheet.addMergedRegion(CellRangeAddress(0, 0, 0, 1))

        // Основная часть

        // Количество файлов, загруженных пользователями
        var rowCount                        = 2
        row                                 = sheet.createRow(rowCount)
        cell                                = row.createCell(0)
        cell.setCellValue("Количество файлов, загруженных пользователями:")
        rowCount++
//        dashboardService.getOrganizationsAndFilesCount(dateFrom, dateTo, workspaceId).forEach {
//            row                             = sheet.createRow(rowCount)
//            cell                            = row.createCell(0)
//            cell.setCellValue(it.first.name)
//            cell                            = row.createCell(1)
//            cell.setCellValue("Всего")
//            cell                            = row.createCell(2)
//            cell.setCellValue(it.second.toDouble())
//            rowCount ++
//            // Если у пользователя есть организация
//            if( it.first.id != null)
//            {
//                dashboardService.getUsersAndFilesCount(dateFrom, dateTo, workspaceId, it.first.id !!).forEach { user ->
//                    row = sheet.createRow(rowCount)
//                    cell = row.createCell(0)
//                    cell = row.createCell(1)
//                    cell.setCellValue(user.first.lastName + " " + user.first.firstName)
//                    cell = row.createCell(2)
//                    cell.setCellValue(user.second.toDouble())
//                    rowCount ++
//                }
//            }
//            // Если у пользователя нет организации
//            else
//            {
//                dashboardService.getUsersAndFilesCountWithoutOrg(dateFrom, dateTo, workspaceId).forEach { user ->
//                    row = sheet.createRow(rowCount)
//                    cell = row.createCell(0)
//                    cell = row.createCell(1)
//                    cell.setCellValue(user.first.lastName + " " + user.first.firstName)
//                    cell = row.createCell(2)
//                    cell.setCellValue(user.second.toDouble())
//                    rowCount ++
//                }
//            }
//
//        }

        sheet.setColumnWidth(0, 8000)
        sheet.setColumnWidth(1, 10000)

        // Создание графика
        val array
                = dashboardService.getDocumentsCountForPeriod(authentication, dateFrom, dateTo, workspaceId)
        rowCount++
//        createLineChart(dateFrom, dateTo, sheet, rowCount, array, "Количество загруженных файлов")
    }

    private fun createUsers(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID,
            sheet                           : XSSFSheet,
            styles                          : HashMap<String, XSSFCellStyle>,
            locale                          : Locale
    )
    {
//        val style                           = styles["TopHeader"]!!
//
//        //Шапка
//        var row                             = sheet.createRow(0)
//        var cell                            = row.createCell(0)
//        cell.setCellValue(messageSource.getMessage("Users", null, locale))
//        cell.cellStyle                      = style
//        row.height                          = 400
//        cell                                = row.createCell(1)
//        cell.cellStyle                      = style
//        sheet.addMergedRegion(CellRangeAddress(0, 0, 0, 1))
//
//        // Основная часть
//        sheet.setColumnWidth(0, 7000)
//        sheet.setColumnWidth(1, 8000)
//
//        var rowCount                        = 1
//        row                                 = sheet.createRow(rowCount)
//        rowCount++
//
        val list
                = dashboardService.getActiveUserCountByOrganization(authentication, dateFrom, dateTo, workspaceId)
        var rownum = 1
        do {
            val row = sheet.createRow(rownum)
            val org = list[rownum].first?.name
            if (org != null) row.createCell(0).setCellValue(org)
            else row.createCell(0).setCellValue("Организация не указана")
            row.createCell(1).setCellValue(list[rownum].second.toDouble())
            rownum++
        } while(rownum < list.size)


    }

    private fun createTasks(
            dateFrom                        : String,
            dateTo                          : String,
            workspaceId                     : UUID,
            sheet                           : XSSFSheet,
            styles                          : HashMap<String, XSSFCellStyle>,
            locale                          : Locale
    )
    {
        val style                           = styles["TopHeader"]

        //Шапка
        var row                             = sheet.createRow(0)
        var cell                            = row.createCell(0)
        cell.setCellValue(messageSource.getMessage("Tasks", null, locale))
        cell.cellStyle                      = style
        row.height                          = 400
        cell                                = row.createCell(1)
        cell.cellStyle                      = style
        sheet.addMergedRegion(CellRangeAddress(0, 0, 0, 1))

        // Основная часть
        row                                 = sheet.createRow(1)
        row                                 = sheet.createRow(2)
        cell                                = row.createCell(0)
        cell.setCellValue("Общее число задач за период" + ":")
        val cellTaskCount                   = row.createCell(1)
        row                                 = sheet.createRow(3)


        sheet.setColumnWidth(0, 7000)
        sheet.setColumnWidth(1, 8000)

//        val organizationList                = dashboardService.getTaskOrganizations(dateFrom, dateTo, workspaceId)
//        val map                             = dashboardService.getTasksStatisticWithOrganizations(dateFrom, dateTo, workspaceId)

        // Формируем шапку
        val tableHeaderStyle                = styles["TableHeader"]
        row                                 = sheet.createRow(4)
        cell                                = row.createCell(0)
        cell.setCellValue("Статус")
        cell.cellStyle                      = tableHeaderStyle
        cell                                = row.createCell(1)
        cell.setCellValue("Общее количество поручений (Нарушение срока)")
        cell.cellStyle                      = tableHeaderStyle
        var cellCount                       = 2
//        organizationList.forEach {
//            cell                            = row.createCell(cellCount)
//            cell.setCellValue(it.name)
//            cell.cellStyle                  = tableHeaderStyle
//            sheet.setColumnWidth(cellCount, 5000)
//            cellCount++
//        }

        // Формируем основную таблицу - список статусов
        val tableCommonStyle                = styles["TableCommon"]
        var rowCount                        = 5

//        statusService.getByTable(null).forEach { status ->
//            var count                       = 0
//            var expored                     = 0
//            cellCount                       = 2
//            row                             = sheet.createRow(rowCount)
//            cell                            = row.createCell(0)
//            cell.setCellValue(messageSource.getMessage(status.name!!, null, locale))
//            cell.cellStyle                  = tableCommonStyle
//            sheet.setColumnWidth(0, 7000)
            // Список организаций
//            organizationList.forEach {
//                // Извлекаем запись из карты
//                val item                    = map[it.id]
//                if (item != null)
//                {
//                    // извлекаем список статусов и количества задач с ними
//                    item.forEach { trio ->
//                        if (status.name.equals(trio.first))
//                        {
//                            // Данные о задачах организации
//                            cell            = row.createCell(cellCount)
//                            cell.setCellValue(trio.second.toString() + " (" + trio.third + ")")
//                            cell.cellStyle  = tableCommonStyle
//                            count           += trio.second
//                            expored         += trio.third
//                        }
//                    }
//                    cellCount++
//                }
//            }
//            cell                            = row.createCell(1)
//            cell.setCellValue(count.toString() + " (" + expored + ")")
//            cell.cellStyle                  = tableCommonStyle
//            rowCount++
//        }

        // Добавляем общий итог по задачам
        row                                 = sheet.createRow(rowCount)
        cell                                = row.createCell(0)
        cell.setCellValue("Общее количество по организациям")
        cell.cellStyle                      = tableCommonStyle
        cell                                = row.createCell(1)
        cell.cellStyle                      = tableCommonStyle
        sheet.addMergedRegion(CellRangeAddress(rowCount, rowCount, 0, 1))
        cellCount                           = 2
        var allTasks                        = 0
//        organizationList.forEach {
//            // Извлекаем запись из карты
//            var taskCount                   = 0
//            var taskExpired                 = 0
//            val item                        = map[it.id]
//            item?.forEach { trio ->
//                taskCount                   += trio.second
//                taskExpired                 += trio.third
//                allTasks                    += trio.second
//            }
//            cell                            = row.createCell(cellCount)
//            cell.setCellValue(taskCount.toString() + " (" + taskExpired + ")")
//            cell.cellStyle                  = tableCommonStyle
//            cellCount++
//        }
        rowCount++

        cellTaskCount.setCellValue(allTasks.toString())
    }


    private fun createOffences(
            authentication                  : OAuth2Authentication,
            dateFrom                        : String,
            dateTo                          : String,
            workspaceId                     : UUID,
            sheet                           : XSSFSheet,
            styles                          : HashMap<String, XSSFCellStyle>,
            locale                          : Locale
    )
    {
        val style                           = styles["TopHeader"]

        //Шапка
        var row                             = sheet.createRow(0)
        var cell                            = row.createCell(0)
        cell.setCellValue(messageSource.getMessage("Offences", null, locale))
        cell.cellStyle                      = style
        row.height                          = 400
        cell                                = row.createCell(1)
        cell.cellStyle                      = style
        sheet.addMergedRegion(CellRangeAddress(0, 0, 0, 1))

        // Основная часть
        row                                 = sheet.createRow(1)
        row                                 = sheet.createRow(2)
        cell                                = row.createCell(0)
        cell.setCellValue("Общее число нарушений за период" + ":")
        val cellTaskCount                   = row.createCell(1)
        row                                 = sheet.createRow(3)

        sheet.setColumnWidth(0, 8000)
        sheet.setColumnWidth(1, 8000)
//
//        val organizationList                = dashboardService.getOffenceOrganizations(dateFrom, dateTo, workspaceId)
//        val map                             = dashboardService.getOffencesStatisticWithOrganizations(dateFrom, dateTo, workspaceId)

        // Формируем шапку
        val tableHeaderStyle                = styles["TableHeader"]
        row                                 = sheet.createRow(4)
        cell                                = row.createCell(0)
        cell.setCellValue("Классификация нарушений")
        cell.cellStyle                      = tableHeaderStyle
        cell                                = row.createCell(1)
        cell.setCellValue("Общее количество нарушений (Устранено)")
        cell.cellStyle                      = tableHeaderStyle
        var cellCount                       = 2
//        organizationList.forEach {
//            cell                            = row.createCell(cellCount)
//            cell.setCellValue(it.name)
//            cell.cellStyle                  = tableHeaderStyle
//            sheet.setColumnWidth(cellCount, 5000)
//            cellCount++
//        }

        // Формируем основную таблицу - список статусов
        val tableCommonStyle                = styles["TableCommon"]
        var rowCount                        = 5

        restTemplate.getOffenceTypeCountByWorkspaceId(authentication, workspaceId).forEach { type ->
            var count                       = 0
            var expored                     = 0
            cellCount                       = 2
            row                             = sheet.createRow(rowCount)
            cell                            = row.createCell(0)
            cell.setCellValue(type.name!!)
            cell.cellStyle                  = tableCommonStyle
            sheet.setColumnWidth(0, 7000)
            // Список организаций
//            organizationList.forEach {
//                // Извлекаем запись из карты
//                val item                    = map[it.id]
//                if (item != null) {
//                    // извлекаем список статусов и количества задач с ними
//                    item.forEach { trio ->
//                        if (type.id!! == trio.first.id)
//                        {
//                            // Данные о задачах организации
//                            cell            = row.createCell(cellCount)
//                            cell.setCellValue(trio.second.toString() + " (" + trio.third + ")")
//                            cell.cellStyle  = tableCommonStyle
//                            count                       += trio.second
//                            expored                     += trio.third
//                        }
//                    }
//                    cellCount++
//                }
//            }
            cell                            = row.createCell(1)
            cell.setCellValue(count.toString() + " (" + expored + ")")
            cell.cellStyle                  = tableCommonStyle
            rowCount++
        }

        // Добавляем общий итог по нарушениям
        row                                 = sheet.createRow(rowCount)
        cell                                = row.createCell(0)
        cell.setCellValue("Общее количество по организациям")
        cell.cellStyle                      = tableCommonStyle
        cell                                = row.createCell(1)
        cell.cellStyle                      = tableCommonStyle
        sheet.addMergedRegion(CellRangeAddress(rowCount, rowCount, 0, 1))
        cellCount                           = 2
        var allTasks                        = 0
//        organizationList.forEach {
//            // Извлекаем запись из карты
//            var taskCount                   = 0
//            var taskExpired                 = 0
//            val item                        = map[it.id]
//            item?.forEach { trio ->
//                taskCount                   += trio.second
//                taskExpired                 += trio.third
//                allTasks                    += trio.second
//            }
            cell                            = row.createCell(cellCount)
//            cell.setCellValue(taskCount.toString() + " (" + taskExpired + ")")
            cell.cellStyle                  = tableCommonStyle
            cellCount++
//        }
//        rowCount++

//        cellTaskCount.setCellValue(allTasks.toString())

    }

    /****************************************************************************************************
     * Формирует линейчатый график в файле Excel.                                                       *
     * @param dateFrom - начало периода.                                                                *
     * @param dateTo - конец периода.                                                                   *
     * @param sheet - лист из файла Excel для вывода списка поручений.                                  *
     * @param rowCount - номер последней строки в формируемом файле.                                    *
     * @param chartData - данные, выводимые на график.                                                  *
     * @param titleText - текст заголовка.                                                              *
     * @return последняя строка
     ***************************************************************************************************/
    private fun createLineChart(
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            sheet                           : XSSFSheet,
            rowCount                        : Int,
            chartData                       : List<Pair<String, Long>>,
            titleText                       : String
    )                                       : Int
    {
        var rowNumber                       = rowCount

        val begin = dateFrom.toString()
        val end = dateTo.toString()

        // Создание таблицы данных
        var row                             = sheet.createRow(rowNumber)
        var cell                            = row.createCell(0)
        cell.setCellValue(titleText)
        rowNumber++
        row                                 = sheet.createRow(rowNumber)
        cell                                = row.createCell(0)
        cell.setCellValue("Дата")
        cell                                = row.createCell(1)
        cell.setCellValue(titleText)
        rowNumber++

        // Если данных нет - вывести что за весь период файлов нет
        if (chartData.isEmpty())
        {
            row                             = sheet.createRow(rowNumber)
            val dateCell                    = row.createCell(0)
            val valueCell                   = row.createCell(1)

            dateCell.setCellValue("$begin - $end")
            valueCell.setCellValue(0.0)
            rowNumber++
            return rowNumber
        }

        val rowDataStart                    = rowNumber
        chartData.forEach {
            row                             = sheet.createRow(rowNumber)
            val dateCell                    = row.createCell(0)
            val valueCell                   = row.createCell(1)

            dateCell.cellType               = CellType.STRING
            dateCell.setCellValue(it.first.toString())

            valueCell.cellType              = CellType.NUMERIC
            valueCell.setCellValue(it.second.toDouble())
            rowNumber++
        }
        val rowDataEnd                      = rowNumber - 1

        val drawing                         = sheet.createDrawingPatriarch()
        // Создание "якоря" - опеределение положения будущего графика и его размеры (в столбцах и рядах()
        val anchor                          = drawing.createAnchor(0, 0, 0, 0,  0, rowNumber+1, 4, rowNumber+14)
        val chart                           = drawing.createChart(anchor)

        // Создание осей
        val bottomAxis                      = chart.createCategoryAxis(AxisPosition.BOTTOM)
        val leftAxis                        = chart.createValueAxis(AxisPosition.LEFT)
        leftAxis.crosses                    = AxisCrosses.AUTO_ZERO

        // Создание данных для осей
        val xs                              = XDDFDataSourcesFactory.fromStringCellRange(sheet, CellRangeAddress(rowDataStart, rowDataEnd, 0,  0))
        val ys                              = XDDFDataSourcesFactory.fromNumericCellRange(sheet, CellRangeAddress(rowDataStart, rowDataEnd, 1, 1))

        val data                            = chart.createData(ChartTypes.LINE, bottomAxis, leftAxis) as XDDFLineChartData
        val series                          = data.addSeries(xs, ys)  as XDDFLineChartData.Series
        chart.plot(data)

        // Настройка внешнего вида графика
        chart.setTitleText(titleText)
        chart.title.body.addNewParagraph()
        chart.titleOverlay                  = false
        // Подписи осей
        leftAxis.setTitle(titleText)
        bottomAxis.setTitle("Дата")
        // Настройка линии графика
        val fill                            = XDDFSolidFillProperties(XDDFColor.from(PresetColor.DARK_BLUE))
        val line                            = XDDFLineProperties()
        line.fillProperties                 = fill
        var properties                      = series.shapeProperties
        if (properties == null)
            properties                      = XDDFShapeProperties()
        properties.lineProperties           = line
        series.shapeProperties              = properties
        // Маркеры на графике
        series.setMarkerStyle(MarkerStyle.CIRCLE)

        // Настройка внешнего вида области графика
        chartSpaceSettings(chart)

        return rowNumber+15
    }

    /****************************************************************************************************
     * Формирует график-пирог в файле Excel.                                                            *
     * @param dateFrom - начало периода.                                                                *
     * @param dateTo - конец периода.                                                                   *
     * @param sheet - лист из файла Excel для вывода списка поручений.                                  *
     * @param rowCount - номер последней строки в формируемом файле.                                    *
     * @param chartData - данные, выводимые на график.                                                  *
     * @param titleText - текст заголовка.                                                              *
     ***************************************************************************************************/
    private fun createPieChart(
            dateFrom                        : String,
            dateTo                          : String,
            sheet                           : XSSFSheet,
            rowCount                        : Int,
            chartData                       : List<Pair<COrganization, Int>>,
            titleText                       : String
    )
    {
        var rowNumber                       = rowCount

        // Создание таблицы данных
        var row                             = sheet.createRow(rowNumber)
        var cell                            = row.createCell(0)
        cell.setCellValue(titleText)
        rowNumber++
        row                                 = sheet.createRow(rowNumber)
        cell                                = row.createCell(0)
        cell.setCellValue("Организация")
        cell                                = row.createCell(1)
        cell.setCellValue(titleText)
        rowNumber++

        // Если данных нет - вывести что за весь период файлов нет
        if (chartData.isEmpty())
        {
            row                             = sheet.createRow(rowNumber)
            val dateCell                    = row.createCell(0)
            val valueCell                   = row.createCell(1)

            dateCell.setCellValue("$dateFrom - $dateTo")
            valueCell.setCellValue(0.0)
            return
        }

        val rowDataStart                    = rowNumber
        chartData.forEach {
            row                             = sheet.createRow(rowNumber)
            val dateCell                    = row.createCell(0)
            val valueCell                   = row.createCell(1)

            dateCell.cellType               = CellType.STRING
            dateCell.setCellValue(it.first.toString())

            valueCell.cellType              = CellType.NUMERIC
            valueCell.setCellValue(it.second.toDouble())
            rowNumber++
        }
        val rowDataEnd                      = rowNumber - 1

        val drawing                         = sheet.createDrawingPatriarch()
        // Создание "якоря" - опеределение положения будущего графика и его размеры (в столбцах и рядах()
        val anchor                          = drawing.createAnchor(0, 0, 0, 0,  0, rowNumber+1, 4, rowNumber+15)
        val chart                           = drawing.createChart(anchor)

        // Создание данных для графика
        val xs                              = XDDFDataSourcesFactory.fromStringCellRange(sheet, CellRangeAddress(rowDataStart, rowDataEnd, 0,  0))
        val ys                              = XDDFDataSourcesFactory.fromNumericCellRange(sheet, CellRangeAddress(rowDataStart, rowDataEnd, 1, 1))

        val data                            = XDDFPieChartData(chart.ctChart.plotArea.addNewPieChart())
        data.setVaryColors(true)
        data.addSeries(xs, ys) as XDDFPieChartData.Series
        chart.plot(data)

        // Настройка внешнего вида графика
        chart.setTitleText(titleText)
        chart.title.body.addNewParagraph()
        chart.titleOverlay                  = false
        // Легенда графика
        val legend                          = chart.orAddLegend
        legend.position                     = LegendPosition.TOP_RIGHT

        // Добавление подписей к данным
        val pieChartPlotArea                = chart.ctChart.plotArea.getPieChartArray(0).getSerArray(0)
        if (!pieChartPlotArea.isSetDLbls)
            pieChartPlotArea.addNewDLbls()
        pieChartPlotArea.dLbls.addNewShowLegendKey().setVal(true)
        pieChartPlotArea.dLbls.addNewShowPercent().setVal(true)

        // Проверка - если подписи автоматически удаляются, то отменить это удаление
        if (chart.ctChart.autoTitleDeleted == null)
            chart.ctChart.addNewAutoTitleDeleted()
        chart.ctChart.autoTitleDeleted.setVal(false)

        // Настройка внешнего вида области графика
        chartSpaceSettings(chart)
    }

    private fun chartSpaceSettings(
            chart                           : XSSFChart
    )
    {
        val ctChartSpace                    = chart.ctChartSpace
        ctChartSpace.addNewRoundedCorners().`val` = false
    }

}