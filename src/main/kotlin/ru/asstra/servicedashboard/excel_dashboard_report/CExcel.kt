package ru.asstra.servicedashboard.excel_dashboard_report

import org.apache.poi.ss.usermodel.*
import org.apache.poi.ss.util.CellRangeAddress
import org.apache.poi.xddf.usermodel.chart.LegendPosition
import org.apache.poi.xddf.usermodel.chart.XDDFDataSourcesFactory
import org.apache.poi.xssf.usermodel.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.stereotype.Service
import ru.asstra.servicedashboard.rest.IRestTemplate
import ru.asstra.servicedashboard.services.IServiceDashboard
import java.awt.Color
import java.io.IOException
import java.time.LocalDate
import java.util.*

@Service
class CExcel : IExcel {
    @Autowired
    lateinit var dashboardService: IServiceDashboard
    @Autowired
    lateinit var restTemplate: IRestTemplate
    @Autowired
    lateinit var messageSource              : MessageSource

    override fun createReportExcel(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID,
            locale                          : Locale
    )                                       : XSSFWorkbook
    {
        val wb = XSSFWorkbook()
        val styles = createStyles(wb)
        val sheet3 = wb.createSheet("Пользователи")
        createUsers(authentication, dateFrom, dateTo, workspaceId, sheet3, styles, locale)
        return wb
    }

    private fun createUsers(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID,
            sheet                           : XSSFSheet,
            styles                          : HashMap<String, XSSFCellStyle>,
            locale                          : Locale
    )
    {
        val style                           = styles["TopHeader"]!!

        //Шапка
        val row                             = sheet.createRow(0)
        val cell                            = row.createCell(0)
        cell.setCellValue(messageSource.getMessage("Organization", null, locale))
        cell.cellStyle                      = style
        row.height                          = 400

        val row2                                 = sheet.createRow(1)
        val cell2                                = row2.createCell(0)
        cell2.cellStyle                      = style
        cell2.setCellValue(messageSource.getMessage("Users", null, locale))

        sheet.setColumnWidth(0, 8000)

        val list
                = dashboardService.getActiveUserCountByOrganization(authentication, dateFrom, dateTo, workspaceId)
        var colnum = 1
        do {
            sheet.setColumnWidth(colnum, 3000)
            var row = sheet.getRow(0)
            val org = list[colnum-1].first?.name
            if (org != null) row.createCell(colnum).setCellValue(org)
            else {
                row.createCell(colnum).setCellValue("Организация не указана")
                sheet.setColumnWidth(colnum, 8000)
            }

            row = sheet.getRow(1)
            val count = list[colnum-1].second.toDouble()
            row.createCell(colnum).setCellValue(count)
            colnum++
        } while(colnum <= list.size)


        val doughnut_drawing = sheet.createDrawingPatriarch()
        val doughnut_anchor = doughnut_drawing.createAnchor(0, 0, 0, 0, 0, 4, 10, 25)

        val doughnut_chart = doughnut_drawing.createChart(doughnut_anchor)
        doughnut_chart.setTitleText("Распределение пользователей по организациям")
        doughnut_chart.titleOverlay = false
        val legend = doughnut_chart.orAddLegend
        legend.position = LegendPosition.TOP_RIGHT

        val cat = XDDFDataSourcesFactory.fromStringCellRange(sheet,
                CellRangeAddress(0, 0, 1, 3))
        val `val` = XDDFDataSourcesFactory.fromNumericCellRange(sheet,
                CellRangeAddress(1, 1, 1, 3))

        val data = CXDDFDoughnutChartData(doughnut_chart, doughnut_chart.ctChart.plotArea.addNewDoughnutChart())
        data.setVaryColors(true)
        data.setHoleSize(64.toShort())
        val series = data.addSeries(cat, `val`)
        doughnut_chart.plot(data)

        // Do not auto delete the title; is necessary for showing title in Calc
        if (doughnut_chart.ctChart.autoTitleDeleted == null) doughnut_chart.ctChart.addNewAutoTitleDeleted()
        doughnut_chart.ctChart.autoTitleDeleted.setVal(false)

        // Data point colors; is necessary for showing data points in Calc
        val pointCount = series.categoryData.pointCount
        for (p in 0 until pointCount) {
            doughnut_chart.ctChart.plotArea.getDoughnutChartArray(0).getSerArray(0).addNewDPt().addNewIdx().setVal(p.toLong())
            doughnut_chart.ctChart.plotArea.getDoughnutChartArray(0).getSerArray(0).getDPtArray(p)
                    .addNewSpPr().addNewSolidFill().addNewSrgbClr().setVal(DefaultIndexedColorMap.getDefaultRGB(p + 10))
        }

        val doughnut_ctdLbls = doughnut_chart.ctChart.plotArea.getDoughnutChartArray(0).getSerArray(0).addNewDLbls()
        doughnut_ctdLbls.addNewShowPercent().setVal(true)
        doughnut_ctdLbls.addNewShowCatName().setVal(false)
        doughnut_ctdLbls.addNewShowLegendKey().setVal(false)
        doughnut_ctdLbls.addNewShowBubbleSize().setVal(false)
        doughnut_ctdLbls.addNewShowSerName().setVal(false)
        doughnut_ctdLbls.addNewShowLeaderLines().setVal(false)
        doughnut_ctdLbls.addNewShowVal().setVal(false)



    }

    /****************************************************************************************************
     * Создаёт таблицу со стилями ячеек.                                                                *
     * @param wb - книга Excel для вывода списка поручений.                                             *
     ***************************************************************************************************/
    private fun createStyles(
            wb: XSSFWorkbook
    ): HashMap<String, XSSFCellStyle> {
        val colorMap = wb.stylesSource.indexedColors
        val white = XSSFColor(Color.WHITE, colorMap)
        val styles = HashMap<String, XSSFCellStyle>()

        var font = wb.createFont()
        font.fontName = "Arial"
        font.bold = true
        font.fontHeight = 200

        //Стиль заголовка.
        var cellStyle = wb.createCellStyle()
        cellStyle.wrapText = true
        cellStyle.verticalAlignment = VerticalAlignment.CENTER
        cellStyle.alignment = HorizontalAlignment.CENTER
        cellStyle.borderBottom = BorderStyle.MEDIUM
        cellStyle.borderLeft = BorderStyle.MEDIUM
        cellStyle.borderTop = BorderStyle.MEDIUM
        cellStyle.borderRight = BorderStyle.MEDIUM
        cellStyle.setFillForegroundColor(white)
        cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND
        cellStyle.setFont(font)
        styles["TopHeader"] = cellStyle

        font = wb.createFont()
        font.fontName = "Arial"
        font.bold = true
        font.setColor(XSSFColor(Color.WHITE, colorMap))
        font.fontHeight = 200

        //Стиль шапки таблицы.
        cellStyle = wb.createCellStyle()
        cellStyle.wrapText = true
        cellStyle.verticalAlignment = VerticalAlignment.BOTTOM
        cellStyle.alignment = HorizontalAlignment.CENTER
        cellStyle.borderBottom = BorderStyle.THIN
        cellStyle.borderLeft = BorderStyle.THIN
        cellStyle.borderTop = BorderStyle.THIN
        cellStyle.borderRight = BorderStyle.THIN
        cellStyle.setLeftBorderColor(white)
        cellStyle.setRightBorderColor(white)
        cellStyle.setTopBorderColor(white)
        cellStyle.setBottomBorderColor(white)
        cellStyle.setFillForegroundColor(XSSFColor(Color(0, 32, 96), colorMap))
        cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND
        cellStyle.setFont(font)

        styles["TableHeader"] = cellStyle

        font = wb.createFont()
        font.fontName = "Arial"
        font.fontHeight = 200

        //Стиль ячеек основной части таблицы.
        cellStyle = wb.createCellStyle()
        cellStyle.wrapText = true
        cellStyle.verticalAlignment = VerticalAlignment.TOP
        cellStyle.alignment = HorizontalAlignment.LEFT
        cellStyle.borderBottom = BorderStyle.THIN
        cellStyle.borderLeft = BorderStyle.THIN
        cellStyle.borderTop = BorderStyle.THIN
        cellStyle.borderRight = BorderStyle.THIN
        cellStyle.setFillForegroundColor(white)
        cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND
        cellStyle.setFont(font)
        styles["TableCommon"] = cellStyle

        font = wb.createFont()
        font.fontName = "Arial"
        font.fontHeight = 200

        //Стиль ячеек по-умолчанию.
        cellStyle = wb.createCellStyle()
        cellStyle.wrapText = true
        cellStyle.verticalAlignment = VerticalAlignment.TOP
        cellStyle.alignment = HorizontalAlignment.LEFT
        cellStyle.setFillForegroundColor(white)
        cellStyle.fillPattern = FillPatternType.SOLID_FOREGROUND
        cellStyle.setFont(font)
        styles["Default"] = cellStyle

        return styles
    }

    fun createFonts(
            wb: XSSFWorkbook
    ): HashMap<String, XSSFFont> {
        val fonts = HashMap<String, XSSFFont>()
        var font = wb.createFont()
        font.italic = true
        font.fontName = "Arial"
        font.fontHeight = 200
        fonts["UserName"] = font

        font = wb.createFont()
        font.fontName = "Arial"
        font.fontHeight = 200
        fonts["Message"] = font

        return fonts
    }
}