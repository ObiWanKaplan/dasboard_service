package ru.asstra.servicedashboard.excel_dashboard_report;

import org.apache.poi.util.Beta;
import org.apache.poi.xddf.usermodel.XDDFShapeProperties;
import org.apache.poi.xddf.usermodel.chart.*;
import org.openxmlformats.schemas.drawingml.x2006.chart.*;

@Beta
public class XDDFDoughnutChartData extends XDDFChartData {
    private CTDoughnutChart chart;

    public XDDFDoughnutChartData(XDDFChart parent, CTDoughnutChart chart) {
        super();
        this.chart = chart;
        for (CTPieSer series : chart.getSerList()) {
            this.series.add(new Series(series, series.getCat(), series.getVal()));
        }
    }


    protected void removeCTSeries(int n) {
        chart.removeSer(n);
    }


    public void setVaryColors(Boolean varyColors) {
        if (varyColors == null) {
            if (chart.isSetVaryColors()) {
                chart.unsetVaryColors();
            }
        } else {
            if (chart.isSetVaryColors()) {
                chart.getVaryColors().setVal(varyColors);
            } else {
                chart.addNewVaryColors().setVal(varyColors);
            }
        }
    }

    public Integer getFirstSliceAngle() {
        if (chart.isSetFirstSliceAng()) {
            return chart.getFirstSliceAng().getVal();
        } else {
            return null;
        }
    }

    public void setFirstSliceAngle(Integer angle) {
        if (angle == null) {
            if (chart.isSetFirstSliceAng()) {
                chart.unsetFirstSliceAng();
            }
        } else {
            if (angle < 0 || 360 < angle) {
                throw new IllegalArgumentException("angle must be between 0 and 360");
            }
            if (chart.isSetFirstSliceAng()) {
                chart.getFirstSliceAng().setVal(angle);
            } else {
                chart.addNewFirstSliceAng().setVal(angle);
            }
        }
    }

    public Short getHoleSize() {
        if (chart.isSetHoleSize()) {
            return chart.getHoleSize().getVal();
        } else {
            return null;
        }
    }



    public void setHoleSize(Short size) {
        if (size == null) {
            if (chart.isSetHoleSize()) {
                chart.unsetHoleSize();
            }
        } else {
            if (size < 0 || 100 < size) {
                throw new IllegalArgumentException("size must be between 0 and 100");
            }
            if (chart.isSetHoleSize()) {
                chart.getHoleSize().setVal(size);
            } else {
                chart.addNewHoleSize().setVal(size);
            }
        }
    }

    @Override
    public void setVaryColors(boolean varyColors) {

    }

    @Override
    public XDDFChartData.Series addSeries(XDDFDataSource<?> category,
                                          XDDFNumericalDataSource<? extends Number> values) {
        final long index = this.series.size();
        final CTPieSer ctSer = this.chart.addNewSer();
        ctSer.addNewCat();
        ctSer.addNewVal();
        ctSer.addNewIdx().setVal(index);
        ctSer.addNewOrder().setVal(index);
        final Series added = new Series(ctSer, category, values);
        this.series.add(added);
        return added;
    }

    public class Series extends XDDFChartData.Series {
        private CTPieSer series;

        protected Series(CTPieSer series, XDDFDataSource<?> category,
                         XDDFNumericalDataSource<? extends Number> values) {
            super(category, values);
            this.series = series;
        }

        protected Series(CTPieSer series, CTAxDataSource category, CTNumDataSource values) {
            super(XDDFDataSourcesFactory.fromDataSource(category), XDDFDataSourcesFactory.fromDataSource(values));
            this.series = series;
        }

        @Override
        protected CTSerTx getSeriesText() {
            if (series.isSetTx()) {
                return series.getTx();
            } else {
                return series.addNewTx();
            }
        }

        @Override
        public void setShowLeaderLines(boolean showLeaderLines) {
            if (!series.isSetDLbls()) {
                series.addNewDLbls();
            }
            if (series.getDLbls().isSetShowLeaderLines()) {
                series.getDLbls().getShowLeaderLines().setVal(showLeaderLines);
            } else {
                series.getDLbls().addNewShowLeaderLines().setVal(showLeaderLines);
            }
        }

        @Override
        public XDDFShapeProperties getShapeProperties() {
            if (series.isSetSpPr()) {
                return new XDDFShapeProperties(series.getSpPr());
            } else {
                return null;
            }
        }

        @Override
        public void setShapeProperties(XDDFShapeProperties properties) {
            if (properties == null) {
                if (series.isSetSpPr()) {
                    series.unsetSpPr();
                }
            } else {
                if (series.isSetSpPr()) {
                    series.setSpPr(properties.getXmlObject());
                } else {
                    series.addNewSpPr().set(properties.getXmlObject());
                }
            }
        }

        public Long getExplosion() {
            if (series.isSetExplosion()) {
                return series.getExplosion().getVal();
            } else {
                return null;
            }
        }

        public void setExplosion(Long explosion) {
            if (explosion == null) {
                if (series.isSetExplosion()) {
                    series.unsetExplosion();
                }
            } else {
                if (series.isSetExplosion()) {
                    series.getExplosion().setVal(explosion);
                } else {
                    series.addNewExplosion().setVal(explosion);
                }
            }
        }

        @Override
        protected CTAxDataSource getAxDS() {
            return series.getCat();
        }

        @Override
        protected CTNumDataSource getNumDS() {
            return series.getVal();
        }

//        @Override
//        protected void setIndex(long val) {
//            series.getIdx().setVal(val);
//        }
//
//        @Override
//        protected void setOrder(long val) {
//            series.getOrder().setVal(val);
//        }
    }
}