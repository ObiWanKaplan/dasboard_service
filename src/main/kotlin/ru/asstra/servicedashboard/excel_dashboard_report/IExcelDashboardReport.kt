package ru.asstra.servicedashboard.excel_dashboard_report

import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.security.oauth2.provider.OAuth2Authentication
import java.time.LocalDate
import java.util.*

interface IExcelDashboardReport
{
    fun createReportExcel(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID,
            locale                          : Locale
    )                                       : XSSFWorkbook
}