package ru.asstra.servicedashboard.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import org.springframework.web.util.UriComponentsBuilder
import ru.asstra.servicedashboard.model.*
import ru.asstra.servicedashboard.model.offences.COffence
import ru.asstra.servicedashboard.model.offences.COffenceType
import ru.asstra.servicedashboard.model.offences.CWorkType
import ru.asstra.servicedashboard.model.tickets.statuses.CStatus
import java.net.URI
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*


/****************************************************************************************************
 * Класс содержит все используемые запросы к API через RestTemplate                                 *
 * @author Каплан Андрей,  13.11.2019.                                                              *
 ***************************************************************************************************/
@Service
class CRestTemplate : IRestTemplate {

    @Autowired
    lateinit var restTemplate               : RestTemplate

    val apiPath                             = "http://localhost:13001/project/api"


    override fun getDocumentsCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Long
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)

        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/dashboard/files/count")
                .queryParam("workspace_id", workspaceId)

        val response = restTemplate
                .exchange(endpoint.toUriString(), HttpMethod.GET, entity, Long::class.java)
        return response.body ?: 0
    }

    override fun getDocumentsTotalSize(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Long
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/dashboard/files/total_size")
                .queryParam("workspace_id", workspaceId)

        val response = restTemplate
                .exchange(endpoint.toUriString(), HttpMethod.GET, entity, Long::class.java)
        return response.body ?: 0
    }

    override fun getOrganization(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            tableId                         : UUID?
    )                                       : Iterable<COrganization>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/organizations")
                .queryParam("workspace_id", workspaceId)
                .queryParam("table_id", tableId)

        val respType                        = object: ParameterizedTypeReference<Iterable<COrganization>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getWorkTypeByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<CWorkType>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/work_type")
                .queryParam("workspace_id", workspaceId)

        val respType                        = object: ParameterizedTypeReference<Iterable<CWorkType>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getTaskByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<CTask>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers = HttpHeaders()
        headers.setBearerAuth(token)
        val entity = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/tasks")
                .queryParam("workspace_id", workspaceId)

        val respType = object: ParameterizedTypeReference<Iterable<CTask>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getOffencesByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<COffence>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/offences")
                .queryParam("workspace_id", workspaceId)

        val respType                        = object: ParameterizedTypeReference<Iterable<COffence>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getCountOffencesByOffenceTypeGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime,
            workTypeId                      : UUID,
            offenceTypeId                   : UUID
    )                                       : MutableList<Tuple>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/offences/getCountByOffenceTypeGroupByOrg")
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)
                .queryParam("worktype_id", workTypeId)
                .queryParam("offencetype_id", offenceTypeId)

        val respType                        = object: ParameterizedTypeReference<MutableList<Tuple>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getCountByOffenceTypeNullGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime,
            workTypeId                      : UUID
    )                                       : MutableList<Tuple>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/offences/getCountByOffenceTypeNullGroupByOrg")
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)
                .queryParam("worktype_id", workTypeId)

        val respType = object: ParameterizedTypeReference<MutableList<Tuple>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getOrganizationById(
            authentication                  : OAuth2Authentication,
            organizationId                  : UUID
    )                                       : Optional<COrganization>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/organizations/getById")
                .queryParam("organization_id", organizationId)

        val respType = object: ParameterizedTypeReference<Optional<COrganization>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getCountByWorkTypeNullGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/offences/getCountByWorkTypeTypeNullGroupByOrg")
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)

        val respType = object: ParameterizedTypeReference<MutableList<Tuple>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getOffenceTypeCountByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<COffenceType>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/offence_type")
                .queryParam("workspace_id", workspaceId)

        val respType = object: ParameterizedTypeReference<Iterable<COffenceType>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun getCountOffencesByOffenceType(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/offences/getCountByOffenceType")
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)

        val respType = object: ParameterizedTypeReference<MutableList<Tuple>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * Возвращает список групп нарушений                                                                *
     * @param workspaceId                                                                               *
     ***************************************************************************************************/

    override fun getWorkTypesByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<CWorkType>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/work_type")
                .queryParam("workspace_id", workspaceId)

        val respType = object: ParameterizedTypeReference<Iterable<CWorkType>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * Возвращает список групп нарушений по типам работ                                                 *
     * @param workspaceId                                                                               *
     * @param dateFrom - дата начала промежутка                                                         *
     * @param dateTo - дата окончания промежутка                                                        *
     ***************************************************************************************************/

    override fun getCountOffencesByWorkType(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/offences/getCountByWorkType")
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)

        val respType = object: ParameterizedTypeReference<MutableList<Tuple>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * Возвращает количество нарушений в проекте                                                        *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun getCountOffencesByActiveTrueAndObjectWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/offences/countByActiveTrueAndObjectWorkspaceId")
                .queryParam("workspace_id", workspaceId)

        val respType = object: ParameterizedTypeReference<Int>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * Возвращает количество поручений в проекте                                                        *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun getCountTasksByActiveTrueAndObjectWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/dashboard/tasks/count")
                .queryParam("workspace_id", workspaceId)

        val response = restTemplate
                .exchange(endpoint.toUriString(), HttpMethod.GET, entity, Int::class.java)

        return response.body ?: 0
    }

    /****************************************************************************************************
     * Запрос возвращает список всех статусов по области                                                *
     * @param tableId - id таблицы, для которой нудно статусы (поручения, нарушения и т.д.)             *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    override fun getStatusesByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            tableId                         : UUID
    )                                       : Iterable<CStatus>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/statuses")
                .queryParam("workspace_id", workspaceId)
                .queryParam("table_id", tableId)

        val respType = object: ParameterizedTypeReference<Iterable<CStatus>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * @param workspaceId - идентификатор рабочей области                                               *
     * @param dateFrom - дата начала промежутка                                                         *
     * @param dateTo - дата окончания промежутка                                                        *
     * @param statusId - идентификатор статуса                                                          *
     ***************************************************************************************************/
    override fun getCountTicketsByStatusGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime,
            statusId                        : UUID
    )                                       : MutableList<Tuple>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/tickets/countByStatusGroupByOrg")
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)
                .queryParam("status_id", statusId)

        val respType = object: ParameterizedTypeReference<MutableList<Tuple>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * @param workspaceId - идентификатор рабочей области                                               *
     * @param dateFrom - дата начала промежутка                                                         *
     * @param dateTo - дата окончания промежутка                                                        *
     * @param statusId - идентификатор статуса                                                          *
     ***************************************************************************************************/
    override fun getCountExpiredTicketsByStatusGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime,
            statusId                        : UUID
    )                                       : MutableList<Tuple>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/tickets/countExpiredByStatusGroupByOrg")
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)
                .queryParam("status_id", statusId)

        val respType = object: ParameterizedTypeReference<MutableList<Tuple>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * @param workspaceId - идентификатор рабочей области                                               *
     * @param dateFrom - дата начала промежутка                                                         *
     * @param dateTo - дата окончания промежутка                                                        *
     ***************************************************************************************************/
    override fun getFindEventCountByUser(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/events/findEventCountByUser")
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)

        val respType = object: ParameterizedTypeReference<MutableList<Tuple>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * Запрос возвращает список всех организаций из репозитория                                         *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    override fun getAllOrganizationsFromRepository(
            authentication                  : OAuth2Authentication
    )                                       : Iterable<COrganization>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/organizations/getAllFromRepository")

        val respType = object: ParameterizedTypeReference<Iterable<COrganization>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * Запрос возвращает список пользователей по рабочей области из репозитория                         *
     * Запрос GET.                                                                                      *
     * @param workspaceId - идентификатор рабочей облоасти
     ***************************************************************************************************/
    override fun getFindUserByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : List<CUser>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/u_w_relation/findUserByWorkspaceId")
                .queryParam("workspace_id", workspaceId)

        val respType = object: ParameterizedTypeReference<List<CUser>>(){}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     * Возвращает количество пользователей рабочей области                                              *
     ***************************************************************************************************/
    override fun getCountByActiveTrueAndWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/dashboard/users/count")
                .queryParam("workspace_id", workspaceId)

        val response = restTemplate
                .exchange(endpoint.toUriString(), HttpMethod.GET, entity, Int::class.java)
        return response.body ?: 0
    }


    /****************************************************************************************************
     * Возвращает количество активных пользователей рабочей области за период                           *
     * @param workspaceId - идентификатор рабочей области                                               *
     * @param dateFrom - дата начала промежутка                                                         *
     * @param dateTo - дата окончания промежутка                                                        *
     ***************************************************************************************************/
    override fun getActiveUserCountByOrganization(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate
    )                                       : List<Pair<COrganization?, Int>>
    {
        val token                           = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/dashboard/users/by_org")
                .queryParam("workspace_id", workspaceId)

        val respType = object: ParameterizedTypeReference<List<Pair<COrganization?, Int>>>(){}

        val response = restTemplate
                .exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType)
        return response.body!!
    }

    /****************************************************************************************************
     * @param workspaceId - идентификатор рабочей области                                               *
     * @param dateFrom - дата начала промежутка                                                         *
     * @param dateTo - дата окончания промежутка                                                        *
     ***************************************************************************************************/
    override fun getFindFileCountByUser(
            authentication                  : OAuth2Authentication,
            itemClass                       : String,
            eventType                       : UUID,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple> {
        val token = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint                        = UriComponentsBuilder
                .fromHttpUrl("$apiPath/events/findFileCountByUser")
                .queryParam("item_class", itemClass)
                .queryParam("event_type", eventType)
                .queryParam("workspace_id", workspaceId)
                .queryParam("date_from", dateFrom)
                .queryParam("date_to", dateTo)

        val respType = object : ParameterizedTypeReference<MutableList<Tuple>>() {}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    /****************************************************************************************************
     *                                                                                                  *
     * @param workspaceId - идентификатор рабочей области                                               *
     * @param dateFrom - дата начала промежутка                                                         *
     * @param dateTo - дата окончания промежутка                                                        *
     ***************************************************************************************************/
    override fun getFindFileCount(
            authentication                  : OAuth2Authentication,
            itemClass                       : String,
            eventType                       : UUID,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime)
                                            : List<Pair<String, String>>
    {
        val token = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers                         = HttpHeaders()
        headers.setBearerAuth(token)
        val entity                          = HttpEntity<Any>(headers)

        val endpoint = UriComponentsBuilder.fromHttpUrl("$apiPath/events/findFileCount")
            .queryParam("item_class", itemClass)
            .queryParam("event_type", eventType)
            .queryParam("workspace_id", workspaceId)
            .queryParam("date_from", dateFrom)
            .queryParam("date_to", dateTo)

        val respType = object : ParameterizedTypeReference<List<Pair<String, String>>>() {}

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }


    override fun getWorkspace(
            authentication                  : OAuth2Authentication,
            id                              : UUID
    )                                       : Optional<CWorkspace>
    {

            val token = (authentication.details as OAuth2AuthenticationDetails).tokenValue
            val headers = HttpHeaders()
            headers.setBearerAuth(token)
            val entity = HttpEntity<Any>(headers)
            /**https://stackoverflow.com/questions/39679180/kotlin-call-java-method-with-classt-argument/39679682#39679682**/
            val endpoint = UriComponentsBuilder.fromHttpUrl("$apiPath/workspaces")
                    .queryParam("id", id)

            val respType = object : ParameterizedTypeReference<Optional<CWorkspace>>() {}

            return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, respType).body!!
    }

    override fun get(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : String
    {
        //Header
        val token = (authentication.details as OAuth2AuthenticationDetails).tokenValue
        val headers = HttpHeaders()
        headers.setBearerAuth(token)
        headers.set("Accept", "application/json")
        val entity = HttpEntity<Any>(headers)

        //URI
        val endpoint = UriComponentsBuilder.fromHttpUrl("$apiPath/dashboard/test")
                .queryParam("workspace_id", workspaceId)

        return restTemplate.exchange(endpoint.toUriString(), HttpMethod.GET, entity, String::class.java).body!!
    }
}