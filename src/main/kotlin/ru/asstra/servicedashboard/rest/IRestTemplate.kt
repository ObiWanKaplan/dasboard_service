package ru.asstra.servicedashboard.rest

import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.stereotype.Service
import ru.asstra.servicedashboard.model.*
import ru.asstra.servicedashboard.model.offences.COffence
import ru.asstra.servicedashboard.model.offences.COffenceType
import ru.asstra.servicedashboard.model.offences.CWorkType
import ru.asstra.servicedashboard.model.tickets.statuses.CStatus
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*
import javax.print.attribute.standard.JobMessageFromOperator

interface IRestTemplate {

    fun getDocumentsCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Long

    fun getDocumentsTotalSize(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Long
    
    fun getOrganization(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            tableId                         : UUID?
    )                                       : Iterable<COrganization>

    fun getWorkTypeByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<CWorkType>

    fun getCountOffencesByOffenceTypeGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime,
            workTypeId                      : UUID,
            offenceTypeId                   : UUID
    )                                       : MutableList<Tuple>

    fun getCountByOffenceTypeNullGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime,
            workTypeId                      : UUID
    )                                       : MutableList<Tuple>

    fun getOrganizationById(
            authentication                  : OAuth2Authentication,
            organizationId                  : UUID
    )                                       : Optional<COrganization>

    fun getCountByWorkTypeNullGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>

    fun getOffenceTypeCountByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<COffenceType>

    fun getCountOffencesByOffenceType(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>

    fun getWorkTypesByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<CWorkType>

    fun getCountOffencesByWorkType(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>

    fun getCountOffencesByActiveTrueAndObjectWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int

    fun getCountTasksByActiveTrueAndObjectWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int

    fun getStatusesByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            tableId                         : UUID
    )                                       : Iterable<CStatus>

    fun getCountTicketsByStatusGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime,
            statusId                        : UUID
    )                                       : MutableList<Tuple>

    fun getCountExpiredTicketsByStatusGroupByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime,
            statusId                        : UUID
    )                                       : MutableList<Tuple>

    fun getFindEventCountByUser(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>

    fun getAllOrganizationsFromRepository(
            authentication                  : OAuth2Authentication
    )                                       : Iterable<COrganization>

    fun getFindUserByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : List<CUser>

    fun getCountByActiveTrueAndWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int

    fun getFindFileCountByUser(
            authentication                  : OAuth2Authentication,
            itemClass                       : String,
            eventType                       : UUID,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime
    )                                       : MutableList<Tuple>

    fun getFindFileCount(
            authentication                  : OAuth2Authentication,
            itemClass                       : String,
            eventType                       : UUID,
            workspaceId                     : UUID,
            dateFrom                        : LocalDateTime,
            dateTo                          : LocalDateTime)
                                            : List<Pair<String, String>>

    fun getWorkspace(
            authentication                  : OAuth2Authentication,
            id                              : UUID
    )                                       : Optional<CWorkspace>

    fun getTaskByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<CTask>

    fun getOffencesByWorkspaceId(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Iterable<COffence>

    fun getActiveUserCountByOrganization(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate
    )                                       : List<Pair<COrganization?, Int>>

    fun get(
            authentication                  : OAuth2Authentication
            ,
            workspaceId: UUID
    ) : String
}