package ru.asstra.servicedashboard.rest

import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.MessageSource
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.*
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.web.bind.annotation.*
import ru.asstra.servicedashboard.excel_dashboard_report.IExcel
import ru.asstra.servicedashboard.excel_dashboard_report.IExcelDashboardReport
import ru.asstra.servicedashboard.model.COrganization
import ru.asstra.servicedashboard.model.CUser
import ru.asstra.servicedashboard.services.IServiceDashboard
import ru.asstra.servicedashboard.services.IServiceExcel
import java.net.URLEncoder
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

@RestController
@RequestMapping("/dashboard")
class CControllerDashboard
{
    @Autowired
    lateinit var serviceDashboard           : IServiceDashboard
    @Autowired
    lateinit var serviceExcel               : IServiceExcel
    @Autowired
    lateinit var service                    : IExcel

//    private var logger                      : Logger
//            = LoggerFactory.getLogger(CStaticFileService::class.java)
    @Autowired
    lateinit var dashboardService           : IServiceDashboard
    @Autowired
    lateinit var excelDashboardReport       : IExcelDashboardReport

    /****************************************************************************************************
     * Объект с переводами текстовых контстант.                                                         *
     ***************************************************************************************************/
    @Autowired
    lateinit var messageSource              : MessageSource

    /****************************************************************************************************
     * Документы                                                                                        *
     ***************************************************************************************************/

    /****************************************************************************************************
     * Возвращает количество загруженных в проект документов - всего                                    *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method = [RequestMethod.GET],
            params = ["workspace_id"],
            path   = ["/files/count"])
    @ResponseBody
    fun getDocumentsCount(
            authentication              : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID)
                                            : Long
    {
        return dashboardService.getDocumentsCount(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает общий размер загруженных в проект документов                                          *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method = [RequestMethod.GET],
            params = ["workspace_id"],
            path   = ["/files/total_size"])
    @ResponseBody
    fun documentsTotalSize(
            authentication              : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                 : UUID
    )       : Long?
    {
        return dashboardService.documentsTotalSize(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает данные для графика-среза за период (количество загруженных и удаленных файлов)        *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method = [RequestMethod.GET],
            params = ["workspace_id", "date_from", "date_to"],
            path   = ["/files"])
    @ResponseBody
    fun getDocumentsCountForPeriod(
            authentication              : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                 : UUID,
            @RequestParam(value             = "date_from")
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            dateFrom                    : LocalDate,
            @RequestParam(value             = "date_to")
            @DateTimeFormat(pattern = "yyyy-MM-dd")
            dateTo                      : LocalDate
    )       : List<Map<String, Any>>
    {
        return dashboardService.getDocumentsCountForPeriod(authentication, dateFrom, dateTo, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает список организаций и количество загруженных ими файлов в организации                  *
     ***************************************************************************************************/
    @RequestMapping(
            method   = [RequestMethod.GET],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE],
            path     = ["/events/files"],
            params   = ["workspace_id", "date_from", "date_to"])
    fun  getFilesCountByUser(
            authentication              : OAuth2Authentication,
            @RequestParam(value             = "date_from")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateFrom                    : LocalDate,
            @RequestParam(value             = "date_to")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateTo                      : LocalDate,
            @RequestParam("workspace_id")
            workspaceId                 : UUID
    )       : Map<String, List<Pair<CUser?, Int>>>
    {
        return dashboardService.getFilesCountByUser(authentication, dateFrom, dateTo, workspaceId)
    }

    /****************************************************************************************************
     * Пользователи                                                                                     *
     ***************************************************************************************************/

    /****************************************************************************************************
     * Возвращает количество пользователей на проекте                                                   *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method = [RequestMethod.GET],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            path   = ["/users/count"],
            params = ["workspace_id"])
    @ResponseBody
    fun getUsersCount(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID)
                                            : Int
    {
        return dashboardService.getUsersCount(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Запрос возвращает количество пользователей по организациям                                       *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method   = [RequestMethod.GET],
            produces = [MediaType.APPLICATION_JSON_VALUE],
            path     = ["/users/by_org"],
            params   = ["workspace_id"])
    @ResponseBody
    fun getUserCountByOrganization(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID
    )                                       : List<Pair<COrganization, Int>>
    {
        return dashboardService.getUserCountByOrg(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Запрос возвращает количесвто активныхпользователей по организациям                               *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method   = [RequestMethod.GET],
            produces = [MediaType.APPLICATION_JSON_UTF8_VALUE],
            path     = ["/users/active_by_org"],
            params   = ["workspace_id", "date_from", "date_to"])
    @ResponseBody
    fun getActiveUserCountByOrganization(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "date_from")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateFrom                        : LocalDate,
            @RequestParam(value             = "date_to")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateTo                          : LocalDate,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID
    )                                       : List<Pair<COrganization?, Int>>
    {
        return dashboardService.getActiveUserCountByOrganization(authentication, dateFrom, dateTo, workspaceId)
    }

    /****************************************************************************************************
     * Заявки (поручения/нарушения)                                                                     *
     ***************************************************************************************************/

    /****************************************************************************************************
     * Возвращает список организаций и данные по общему и просроченному количеству заявок               *
     * в разрезе статуса и организации                                                                  *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method                          = [RequestMethod.GET],
            params                          = ["workspace_id", "date_from", "date_to", "table_id"],
            path                            = ["/tickets/status_and_org"])
    @ResponseBody
    fun getCountTicketsByOrgAndStatus(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "date_from")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateFrom                        : LocalDate,
            @RequestParam(value             = "date_to")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateTo                          : LocalDate,
            @RequestParam("workspace_id")
            workspaceId                     : UUID,
            @RequestParam(value             = "table_id")
            tableId                         : UUID
    )       : Map<String, List<Any>>
    {
        return dashboardService.getCountTicketsByOrgAndStatus(authentication, workspaceId, dateFrom, dateTo, tableId)
    }

    /****************************************************************************************************
     * Поручения                                                                                        *
     ***************************************************************************************************/

    /****************************************************************************************************
     * Возвращает количество поручений на проекте                                                       *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method = [RequestMethod.GET],
            params = ["workspace_id"],
            path   = ["/tasks/count"])
    @ResponseBody
    fun getTasksCount(
            authentication              : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID)
            : Int
    {
        return dashboardService.getTasksCount(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Нарушения                                                                                        *
     ***************************************************************************************************/

    /****************************************************************************************************
     * Возвращает количество нарушений на проекте                                                       *
     * Запрос GET.                                                                                      *
//     ***************************************************************************************************/
    @RequestMapping(
            method = [RequestMethod.GET],
            params = ["workspace_id"],
            path   = ["/offences/count"])
    @ResponseBody
    fun getOffencesCount(
            authentication              : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID)
            : Int
    {
        return dashboardService.getOffencesCount(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает количество нарушений за период в разрезе групп нарушений                              *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method                          = [RequestMethod.GET],
            params                          = ["workspace_id", "date_from", "date_to"],
            path                            = ["/offences/work_type"])
    @ResponseBody
    fun getCountOffencesByWorkType(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "date_from")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateFrom                        : LocalDate,
            @RequestParam(value             = "date_to")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateTo                          : LocalDate,
            @RequestParam("workspace_id")
            workspaceId                     : UUID)
            : List<Map<String, Any>>
    {
        return dashboardService.getCountOffencesByWorkType(authentication, dateFrom, dateTo, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает количество нарушений за период в разрезе классов нарушений                            *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method                          = [RequestMethod.GET],
            params                          = ["workspace_id", "date_from", "date_to"],
            path                            = ["/offences/offence_type"])
    @ResponseBody
    fun getCountOffencesByOffenceType(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "date_from")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateFrom                        : LocalDate,
            @RequestParam(value             = "date_to")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateTo                          : LocalDate,
            @RequestParam("workspace_id")
            workspaceId                     : UUID)
            : List<Map<String, Any>>
    {
        return dashboardService.getCountOffencesByOffenceType(authentication, workspaceId, dateFrom, dateTo)
    }

    /****************************************************************************************************
     * Возвращает список организаций и данные по общему количеству нарушений                            *
     * в разрезе типов нарушений и организации                                                          *
     * Запрос GET.                                                                                      *
     ***************************************************************************************************/
    @RequestMapping(
            method                          = [RequestMethod.GET],
            params                          = ["workspace_id", "date_from", "date_to", "table_id"],
            path                            = ["/offences/type_and_org"])
    @ResponseBody
    fun getCountOffencesByOrgAndTypes(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "date_from")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateFrom                        : LocalDate,
            @RequestParam(value             = "date_to")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateTo                          : LocalDate,
            @RequestParam("workspace_id")
            workspaceId                     : UUID,
            @RequestParam("table_id")
            tableId                         : UUID)
            : Map<String, List<Any>>
    {
        return dashboardService.getCountOffencesByOrgAndTypes(authentication, dateFrom, dateTo, workspaceId, tableId)
    }

    /****************************************************************************************************
     * По переданному фильтру собирает отчет, выводит его в файл Excel и возвращает его в ответе.       *
     * Запрос POST.                                                                                     *
     * @param workspaceId - идентификатор рабочей области.                                              *
     * @param locale - языу, которые браузер берёт из клиентской системы, установлен в заголовке        *
     * @param browser - текстовое описание браузера, для корректного наименования ответного файла.      *
     *  запроса "Accept-Language", для перевода специфических текстовых констант в файле.               *
     ***************************************************************************************************/
//    @RequestMapping(
//            method                          = [RequestMethod.POST],
//            params                          = ["workspace_id", "browser", "date_from", "date_to"],
//            path                            = ["/download"])
//    @ResponseBody
//    fun downloadReport(
//            authentication              : OAuth2Authentication,
//            @RequestParam(value             = "date_from")
//            dateFrom                    : String,
//            @RequestParam(value             = "date_to")
//            dateTo                      : String,
//            @RequestParam(value             = "workspace_id")
//            workspaceId                 : UUID,
//            locale                          : Locale,
//            @RequestParam(value             = "browser")
//            browser                     : String
//    )       : HttpEntity<ByteArray>
//    {
//        //Формируем файл.
//        val wb                              = excelDashboardReport
//                .createReportExcel(authentication, dateFrom, dateTo, workspaceId, locale)
//        //Файл переводим в массив байт.
//        val bos                             = java.io.ByteArrayOutputStream()
//        bos.use{ wb.write(it) }
//        val bytes                           = bos.toByteArray()
//        //Готовим ответ браузеру.
//        val filename                        = URLEncoder.encode(messageSource.getMessage("Project status report", null, locale) + ".xlsx", "UTF-8")
//        val headers                         = HttpHeaders()
//        headers.set("Access-Control-Expose-Headers", "Content-Disposition")
//        headers.set("Content-Type", "application/vnd.ms-excel;")
//        headers.set("content-length",  bytes.size.toString())
//        when(browser)
//        {
//            "Firefox" -> headers.set("Content-Disposition", "attachment; filename*=UTF-8''$filename")
//            else -> headers.set("Content-Disposition", "attachment; filename=$filename")
//        }
////        logger.info("Excel created.")
//        return ResponseEntity(bytes, headers, HttpStatus.CREATED)
//    }


    /****************************************************************************************************
     * Запрос возвращает список организаций для администратора                                          *
     * Запрос GET.                                                                                      *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    @RequestMapping(
            method                          = [RequestMethod.GET],
            produces                        = [MediaType.APPLICATION_JSON_VALUE],
            params                          = ["workspace_id", "table_id"])
    @ResponseBody
    fun getAll(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID,
            @RequestParam(value             = "table_id")
            tableId                         : UUID
    )                                       : List<COrganization>
    {
                return serviceDashboard.getOrganizations(authentication, workspaceId, tableId)
    }


    /****************************************************************************************************
     * Запрос возвращает документ excel                                                                 *
     * Запрос POST.                                                                                      *
     ***************************************************************************************************/

//    @RequestMapping(
//            method                          = [RequestMethod.POST],
//            path                            = ["/download"])
//    @ResponseBody
//    fun downloadReport()                    : HttpEntity<ByteArray>
//    {
//        //Формируем файл.
//        val wb                              = XSSFWorkbook()
//        val sheet = wb.createSheet("Sheet")
//        val row = sheet.createRow(0)
//        row.createCell(0).setCellValue("O")
//        row.createCell(1).setCellValue("P")
//        row.createCell(2).setCellValue("A")
//        //Файл переводим в массив байт.
//        val bos                             = java.io.ByteArrayOutputStream()
//        bos.use{ wb.write(it) }
//        val bytes                           = bos.toByteArray()
//        //Готовим ответ браузеру.
//       // val filename                        = URLEncoder.encode(messageSource.getMessage("Project status report", null, locale) + ".xlsx", "UTF-8")
//        val headers                         = HttpHeaders()
//        headers.set("Access-Control-Expose-Headers", "Content-Disposition")
//        headers.set("Content-Type", "application/vnd.ms-excel;")
//        headers.set("content-length",  bytes.size.toString())
//
//        return ResponseEntity(bytes, headers, HttpStatus.CREATED)
//    }

    /****************************************************************************************************
     * По переданному фильтру собирает отчет, выводит его в файл Excel и возвращает его в ответе.       *
     * Запрос POST.                                                                                     *
     * @param workspaceId - идентификатор рабочей области.                                              *
     * @param locale - языу, которые браузер берёт из клиентской системы, установлен в заголовке        *
     * @param browser - текстовое описание браузера, для корректного наименования ответного файла.      *
     *  запроса "Accept-Language", для перевода специфических текстовых констант в файле.               *
     ***************************************************************************************************/
    @RequestMapping(
            method                          = [RequestMethod.POST],
            params                          = ["workspace_id", "browser", "date_from", "date_to"],
            path                            = ["/download"])
    @ResponseBody
    fun downloadReport(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "date_from")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateFrom                        : LocalDate,
            @RequestParam(value             = "date_to")
            @DateTimeFormat(pattern         = "yyyy-MM-dd")
            dateTo                          : LocalDate,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID,
            locale                          : Locale,
            @RequestParam(value             = "browser")
            browser                         : String
    )       : HttpEntity<ByteArray>
    {
        //Формируем файл.
        val wb   = service.createReportExcel(authentication, dateFrom, dateTo, workspaceId, locale)
        //Файл переводим в массив байт.
        val bos                             = java.io.ByteArrayOutputStream()
        bos.use{ wb.write(it) }
        val bytes                           = bos.toByteArray()
        //Готовим ответ браузеру.
        val filename                        = URLEncoder.encode(messageSource.getMessage("Project status report", null, locale) + ".xlsx", "UTF-8")
        val headers                         = HttpHeaders()
        headers.set("Access-Control-Expose-Headers", "Content-Disposition")
        headers.set("Content-Type", "application/vnd.ms-excel;")
        headers.set("content-length",  bytes.size.toString())
        when(browser)
        {
            "Firefox" -> headers.set("Content-Disposition", "attachment; filename*=UTF-8''$filename")
            else -> headers.set("Content-Disposition", "attachment; filename=$filename")
        }
       // logger.info("Excel created.")
        return ResponseEntity(bytes, headers, HttpStatus.CREATED)
    }


    /****************************************************************************************************
     * Запрос возвращает список организаций для администратора                                          *
     * Запрос GET.                                                                                      *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    @RequestMapping(
            method                          = [RequestMethod.GET],
            produces                        = [MediaType.APPLICATION_JSON_VALUE],
            path                            = ["/test"],
            params                          = ["workspace_id"]
            )
    @ResponseBody
    fun test(
            authentication                  : OAuth2Authentication,
            @RequestParam(value             = "workspace_id")
            workspaceId                     : UUID
    )                                       : String
    {
        return serviceDashboard.get(authentication, workspaceId)
    }
}