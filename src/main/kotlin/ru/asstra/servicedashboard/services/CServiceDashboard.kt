package ru.asstra.servicedashboard.services

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.oauth2.provider.OAuth2Authentication
import org.springframework.stereotype.Service
import ru.asstra.servicedashboard.model.COrganization
import ru.asstra.servicedashboard.model.CUser
import java.util.*
import ru.asstra.servicedashboard.model.offences.COffenceType
import ru.asstra.servicedashboard.model.offences.CWorkType
import ru.asstra.servicedashboard.rest.IRestTemplate
import ru.asstra.servicedashboard.utils.EventType
import java.math.BigInteger
import java.time.Duration
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.math.roundToLong


/**************************************************************************************************************
 * Класс реализует все методы бизнес-логики, которые могут применятся для формирования данных в виде дашборда *
 * @author Теплюк О.В. 2019 0725                                                                              *
 **************************************************************************************************************/
@Service
class CServiceDashboard                     : IServiceDashboard
{
    @Autowired
    lateinit var restTemplate               : IRestTemplate
    /****************************************************************************************************
     * Возвращает количество загруженных документов в проекте (рабочей области)                         *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun getDocumentsCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Long
    {
        return restTemplate.getDocumentsCount(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает количество загруженных и удаленных документов в проекте (рабочей области) за период   *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun getDocumentsCountForPeriod(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID
    )                                       : List<Map<String, Any>>
    {
        // Установим количество отрезков на графике
        val n                               = 10
        // Нам нужно разбить период так, что получилось не более 10 точек.
        // Найдем количество дней между началом и концом периода
        val countDay                        = Duration.between(dateFrom.atStartOfDay(), dateTo.atStartOfDay()).toDays() + 1

        // Найдем шаг, которым будем делить период (кол-во дней)
        val step                            = if (countDay < n) 1 else countDay.div(n.toDouble()).roundToLong()

        // Переводим даты в LocalDateTime
        var start                           = dateFrom.atStartOfDay() // Начало дня
        val final                           = dateTo.atStartOfDay().plusDays(1) // Начало дня следующего дня

        // Берем из БД данные за полный период
        val added                           = mutableMapOf<Int, Long>()
        val formatter                       = DateTimeFormatter.ofPattern("yyyy-MM-dd")
        // Данные придут в формате <Date, BigInteger> - количество событий добавления за определенную дату
        // !!! Используется nativeQuery, потому что не получилось типизировать item, как CFile и, в итоге, подтягиваются все таблицы !!!
        restTemplate.getFindFileCount(authentication, "CFile", EventType.CREATE.id, workspaceId, start, final)
                // Пройдем по полученному списку кортежей, и соберем карту Map<position, count> - определим события по интервалу
                .forEach {
                    val position            = positionByStep(LocalDate.parse(it.first, formatter).atStartOfDay(), start, step)
                    added[position]         = added[position]?.plus(it.second.toLong()) ?: it.second.toLong()
                }
        // Берем данные о деактивированных файлах
        val removed                         = mutableMapOf<Int, Long>()
        restTemplate.getFindFileCount(authentication, "CFile", EventType.DEACTIVATE.id, workspaceId, start, final)
                // Пройдем по полученному списку кортежей, и соберем карту Map<position, count> - определим события по интервалу
                .forEach {
                    val position            = positionByStep(LocalDate.parse(it.first, formatter).atStartOfDay(), start, step)
                    removed[position]       = removed[position]?.plus(it.second.toLong()) ?: it.second.toLong()
                }

        val result                          = mutableListOf<Map<String, Any>>()
        var i                               = 0
        // В цикле начинаем двигаться по периоду с найденным шагом
        // Если при добавлении шага к началу, оно становится позже конца периода, то выходим
        while (start.plusDays(step).isBefore(final) || start.plusDays(step).isEqual(final))
        {
            // Вычисляем конец
            val end                         = start.plusDays(step)
            // Создаем карту и пишет туда период
            val map                         = mutableMapOf<String, Any>()
            map["start"]                    = start.format(formatter)
            map["end"]                      = end.format(formatter)
            // Находим количество добавленных файлов за период
            map["add"]                      = if (added.containsKey(i)) added[i]!! else 0
            // Находим количество деактивированных файлов за период
            map["remove"]                   = if (removed.containsKey(i)) removed[i]!! else 0
            result.add(map)
            i++
            start                           = end
        }
        return result
    }

    private fun positionByStep(
            curDate                         : LocalDateTime,
            start                           : LocalDateTime,
            step                            : Long)
            : Int
    {
        val count                           = Duration.between(start, curDate).toDays()
        return count.div(step).toInt()
    }

    /****************************************************************************************************
     * Возвращает общий размер загруженных в проект документов                                          *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun documentsTotalSize(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Long
    {
        return restTemplate.getDocumentsTotalSize(authentication, workspaceId)
    }

    /*************************************************************************************************************
     * Список организаций и количество загруженных ими файлов                                                  *
     *************************************************************************************************************/
    override fun getFilesCountByUser(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID
    )                                       : Map<String, List<Pair<CUser?, Int>>>
    {
        val begin                           = dateFrom.atStartOfDay()
        val end                             = dateTo.atStartOfDay().plusDays(1)

        // Получаем количество загруженный файлов за период
        val added                           = mutableListOf<Pair<CUser?, Int>>()
        restTemplate.getFindFileCountByUser(authentication,"CFile", EventType.CREATE.id, workspaceId, begin, end)
                .forEach {
                    // По результатам запроса заполняем список
                    added.add(Pair<CUser?, Int>(it[0, CUser::class.java], it[1, java.lang.Long::class.java].toInt()))
                }

        // Получаем количество удаленных файлов за период
        val removed                         = mutableListOf<Pair<CUser?, Int>>()
        restTemplate.getFindFileCountByUser(authentication, "CFile", EventType.DEACTIVATE.id, workspaceId, begin, end)
                .forEach{
                    removed.add(Pair<CUser?, Int>(it[0, CUser::class.java], it[1, java.lang.Long::class.java].toInt()))
                }

        val result                          = mutableMapOf<String, List<Pair<CUser?, Int>>>()

        result["added"]                     = added
        result["removed"]                   = removed

        return result
    }

    /****************************************************************************************************
     * Возвращает количество пользователей в проекте                                                    *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun getUsersCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int
    {
        return restTemplate.getCountByActiveTrueAndWorkspaceId(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает список организаций и количество пользователей в них                                   *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun getUserCountByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : List<Pair<COrganization, Int>>
    {
        // Берем список организаций
        val organizations = restTemplate.getAllOrganizationsFromRepository(authentication)
        // и запишем их в карту
        val orgMap = hashMapOf<UUID, COrganization>()
        organizations.forEach { org ->
            orgMap[org.id!!] = org
        }

        // Находим количество пользователей по организациям (идентификатор)
        val byOrgIdList                         = restTemplate.getFindUserByWorkspaceId(authentication, workspaceId)
                .groupingBy { it.organization?.id }
                .eachCount()
                .toList()

        // На основе найденных данных заполняем карту, подтягивая по id организацию из карты
        val byOrgList = mutableListOf<Pair<COrganization, Int>>()
        byOrgIdList.forEach { byOrg ->
            byOrgList.add(Pair(
                    if (byOrg.first != null) orgMap[byOrg.first!!]!! else COrganization(),
                    byOrg.second
            ))
        }
        return byOrgList
    }

    /****************************************************************************************************
     * Возвращает список организаций и количество активных пользователей в них                          *
     ***************************************************************************************************/
    override fun getActiveUserCountByOrganization(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID)
                                            : List<Pair<COrganization?, Int>>
    {

        return restTemplate.getActiveUserCountByOrganization(authentication, workspaceId, dateFrom, dateTo)

//        val byOrgs                          = mutableMapOf<String, Pair<COrganization?, Int>>()
//
//        // Берем все организации области и заполняем карту найденными организациями
//        restTemplate.getOrganization(authentication, workspaceId, null)
//                .forEach {org ->
//                    byOrgs[org.id.toString()] = Pair(org, 0)
//                }
//
//        restTemplate.getFindEventCountByUser(authentication, workspaceId, begin, end)
//                .forEach {
//                    // Действия системы мы не учитываем
//                    if (it[0, CUser::class.java] !== null && it[0, CUser::class.java].id == UUID.fromString("dd252c63-37d5-459d-be72-15feab594a0f"))
//                        return@forEach
//                    // Для каждого пользователя смотрим его организацию.
//                    // Note! Если у пользователя не указана организация, то указывается пустой объект организации (в карте ключ "null")
//                    // Если мы не знаем, что это за пользователь, то на месте организации ставим null(в карте ключ "userNull")
//                    val org                 = if (it[0, CUser::class.java] == null) null else it[0, CUser::class.java].organization ?: COrganization()
//                    val orgId               = if (org == null) "userNull" else org.id?.toString() ?: "null"
//                    // Учитываем пользователя в карте организаций
//                    if (byOrgs.containsKey(orgId))
//                        byOrgs[orgId] = byOrgs[orgId]!!.copy(second = byOrgs[orgId]!!.second + 1)
//                    else
//                        byOrgs[orgId]       = Pair(org, 1)
//                }
//        return byOrgs.values.toList()
    }

    /****************************************************************************************************
     * Возвращает список организаций и данные по общему и просроченному количеству заявок               *
     * в разрезе статуса и организации                                                                  *
     ***************************************************************************************************/
    /** Итоговая структура, которая отсылается
    {
        organizations: [], // Список организаций
        countsByStatus: // Данные о количестве по статусам и организациям
        [{
            status: {}, // Статус
            all: Int, // Кол-во всего по статусу
            expired: Int, // Кол-во просроченных по статусу
            countsByOrganization: // Данные о кол-ве по организациям
            {
                <organizationId>: , // Идентификатор организации или null, если не определена
                {
                    all: Int, // Кол-во всего по статусу и организации
                    expired: Int // Кол-во просроченных по статусу и организации
                }
            }
        }, ...]
    }
    **/
    override fun getCountTicketsByOrgAndStatus(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            tableId                         : UUID
    )                                       : Map<String, List<Any>>
    {
        val begin                           = dateFrom.atStartOfDay()
        val end                             = dateTo.atStartOfDay().plusDays(1)

        // Список организаций
        val organizations
                = restTemplate.getOrganization(authentication, workspaceId, tableId).toMutableList()

        // Список статусов
        val statuses              = restTemplate.getStatusesByWorkspaceId(authentication, workspaceId, tableId)

        // Результирующий список
        val result                          = mutableMapOf<String, List<Any>>()

        val countsByStatus                  = mutableListOf<Map<String, Any>>()

        // Для каждого статуса
        statuses.forEach {status ->
            // Записываем основные значения по статусу
            val countByStatus               = mutableMapOf<String, Any>()
            countByStatus["status"]         = status
            countByStatus["all"]            = 0
            countByStatus["expired"]        = 0

            val countsByOrg                 = mutableMapOf<String, MutableMap<String, Int>>()

            // Получаем общее количество заявок по организациям
            restTemplate.getCountTicketsByStatusGroupByOrg(authentication, workspaceId, begin, end, status.id!!)
                    .forEach {
                        // Заполняем найденными значениями карту по организациям.
                        // Берем id организации
                        val orgId           = it[0]?.toString() ?: "null"
                        // Заполняем значение по организации
                        val count           = mutableMapOf<String,Int>()
                        count["all"]        = it[1, BigInteger::class.java].toInt()
                        // Учитываем в общем числе по статусу
                        countByStatus["all"] = countByStatus["all"] as Int + it[1, BigInteger::class.java].toInt()
                        // Добавляем в карту по организациям
                        countsByOrg[orgId]  = count

                        // Для каждой найденной организации проверяем, чтобы она была в общем списке.
                        if (!organizations.any { org -> org.id == if (orgId == "null") null else UUID.fromString(orgId) })
                        {
                            // Обрабатываем организацию с "null
                            if (orgId == "null")
                                organizations.add(0, COrganization())
                            else
                            {
                                // Ищем организацию по id в БД и добавляем в список
                                val orgOpt  = restTemplate.getOrganizationById(authentication, UUID.fromString(orgId))
                                if (orgOpt.isPresent)
                                    organizations.add(orgOpt.get())
                            }
                        }
                    }
            // Получаем количетсво просроченных заявок по организациям
            restTemplate.getCountExpiredTicketsByStatusGroupByOrg(authentication, workspaceId, begin, end, status.id!!)
                    .forEach {
                        // Для просроченных запись в карте уже создана, так как просроченные учитываются в общем количестве
                        // Берем id организации
                        val orgId = it[0]?.toString() ?: "null"
                        // Записываем значение
                        countsByOrg[orgId]!!["expired"] = it[1, BigInteger::class.java].toInt()
                        // Учитываем в общем числе по статусу
                        countByStatus["expired"] = countByStatus["expired"] as Int + it[1, BigInteger::class.java].toInt()
                    }

            countByStatus["countsByOrganization"] = countsByOrg
            // Добавляем в список данных по статусу
            countsByStatus.add(countByStatus)
        }
        // Заполняем результирующую карту
        result["organizations"]             = organizations
        result["countsByStatus"]            = countsByStatus
        return result
    }

    /****************************************************************************************************
     * Возвращает количество поручений в проекте                                                        *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun getTasksCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int
    {
        return restTemplate.getCountTasksByActiveTrueAndObjectWorkspaceId(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает количество нарушений в проекте                                                        *
     * @param workspaceId - идентификатор рабочей области.                                              *
     ***************************************************************************************************/
    override fun getOffencesCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID
    )                                       : Int
    {
        return restTemplate.getCountOffencesByActiveTrueAndObjectWorkspaceId(authentication, workspaceId)
    }

    /****************************************************************************************************
     * Возвращает количество нарушений в проекте за определенный период в разрезе типов нарушений       *
     ***************************************************************************************************/
    override fun getCountOffencesByWorkType(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID)
                                            : List<Map<String, Any>>
    {
        val begin                           = dateFrom.atStartOfDay()
        val end                             = dateTo.atStartOfDay().plusDays(1)

        // Список групп нарушений
        val workTypes
                = restTemplate.getWorkTypesByWorkspaceId(authentication, workspaceId).toMutableList()

        val result                          = mutableListOf<Map<String, Any>>()

        // Получаем количество нарушений по типам работ
        restTemplate.getCountOffencesByWorkType(authentication, workspaceId, begin, end)
                .forEach {
                    // Заполняем найденными значениями карту по типам нарушений.
                    val byWorkType          = mutableMapOf<String, Any>()
                    // Берем id типа нарушений
                    val workTypeId          = it[0]?.toString() ?: "null"
                    if (workTypeId == "null")
                    {
                        // Пустой объект типа нарушений
                        byWorkType["workType"] = CWorkType()
                        // Количество нарушений данного типа
                        byWorkType["count"] = it[1, BigInteger::class.java].toInt()
                    }
                    else
                    {
                        // Ищем объект в списке типов
                        byWorkType["workType"] = workTypes.find { type -> type.id == UUID.fromString(workTypeId) }
                                ?: return@forEach
                        // Количество нарушений данного типа
                        byWorkType["count"] = it[1, BigInteger::class.java].toInt()
                        // Убираем найденный тип из списка
                        workTypes.removeIf { type -> type.id == UUID.fromString(workTypeId) }
                    }
                    result.add(byWorkType)
                }
        // Добавляем все оставшиеся типы с количеством 0
        workTypes.forEach {
            val byWorkType                  = mutableMapOf<String, Any>()
            byWorkType["workType"]          = it
            byWorkType["count"]             = 0
            result.add(byWorkType)
        }
        return result
    }

    /****************************************************************************************************
     * Возвращает количество нарушений в проекте за определенный период в разрезе классов нарушений     *
     ***************************************************************************************************/
    override fun getCountOffencesByOffenceType(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate
    )                                       : List<Map<String, Any>>
    {
        val begin                           = dateFrom.atStartOfDay()
        val end                             = dateTo.atStartOfDay().plusDays(1)

        val offenceTypes
                = restTemplate.getOffenceTypeCountByWorkspaceId(authentication, workspaceId)
        //val offenceTypes                    = serviceOffenceType.getByWorkspaceId(workspaceId)

        val result                          = mutableListOf<Map<String, Any>>()

        // Получаем количество нарушений по типам работ
        restTemplate.getCountOffencesByOffenceType(authentication, workspaceId, begin, end)
                .forEach {
                    // Заполняем найденными значениями карту по типам нарушений.
                    val byOffenceType       = mutableMapOf<String, Any>()
                    // Берем id класса нарушений
                    val offenceTypeId       = it[0]?.toString() ?: "null"
                    if (offenceTypeId == "null")
                    {
                        // Пустой объект типа нарушений
                        byOffenceType["offenceType"] = COffenceType()
                        // Количество нарушений данного типа
                        byOffenceType["count"] = it[1, BigInteger::class.java].toInt()
                    }
                    else
                    {
                        // Ищем объект в списке типов
                        byOffenceType["offenceType"] = offenceTypes.find { type -> type.id == UUID.fromString(offenceTypeId) }
                                ?: return@forEach
                        // Количество нарушений данного типа
                        byOffenceType["count"] = it[1, BigInteger::class.java].toInt()
                    }
                    result.add(byOffenceType)
                }

        return result
    }

    /****************************************************************************************************
     * Возвращает список организаций и данные по общему количеству нарушений                            *
     * в разрезе групп и классов нарушений и организации                                                *
     ***************************************************************************************************/
    /** Итоговая структура, которая отсылается
    {
        organizations: [], // Список организаций
        countsByType: // Данные о количестве по типам нарушений и организациям
        [{
            type: {}, // Статус
            isWorkType: Boolean, // Является ли тип - группой нарушений
            all: Int, // Кол-во всего по типу
            countsByOrganization: // Данные о кол-ве по организациям
            {
                <organizationId>: , // Идентификатор организации или null, если не определена
                {
                    all: Int, // Кол-во всего по типу и организации
                }
            }
        }, ...]
    }
    **/
    override fun getCountOffencesByOrgAndTypes(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID,
            tableId                         : UUID?
    )                                       : Map<String, List<Any>>
    {
        val begin                           = dateFrom.atStartOfDay()
        val end                             = dateTo.atStartOfDay().plusDays(1)

        // Список организаций
        val organizations = restTemplate.getOrganization(authentication, workspaceId, tableId).toMutableList()

        // Список групп нарушений
        val workTypes
                = restTemplate.getWorkTypeByWorkspaceId(authentication, workspaceId).toList()
        //val workTypes                       = serviceWorkType.getByWorkspaceId(workspaceId).toList()

        // Результирующий список
        val result                          = mutableMapOf<String, List<Any>>()

        val countsByType                    = mutableListOf<Map<String, Any>>()

        // Для каждой группы нарушений
        workTypes.forEach {workType ->
            // Записываем основные значения по группе
            val countByWorkType             = mutableMapOf<String, Any>()
            countByWorkType["type"]         = workType
            countByWorkType["isWorkType"]   = true
            countByWorkType["all"]          = 0

            val countsByOrgForWorkType      = mutableMapOf<String, MutableMap<String, Int>>()

            val offenceTypesTemp            = mutableListOf<Map<String, Any>>()

            // Для каждого класса нарушений
            // Берем только активные связи и из них достаем класс нарушений
            val offenceTypes                = workType.offenceTypes.filter { it.active }.map { it.offenceType!! }.toMutableList()
            // Добаляем в список пустой класс, для случая, когда установлена группа, но класс не установлен
            offenceTypes.add(COffenceType())
            offenceTypes.forEach offenceType@{ offenceType ->
                // Записываем основные значения по классификации
                val countByOffenceType      = mutableMapOf<String, Any>()
                countByOffenceType["type"]  = offenceType
                countByOffenceType["isWorkType"] = false
                countByOffenceType["all"]   = 0

                val countsByOrgForOffenceType = mutableMapOf<String, MutableMap<String, Int>>()

                // Получаем общее количество нарушений по организациям
                val byOrg                   = if (offenceType.id != null)
                    restTemplate.getCountOffencesByOffenceTypeGroupByOrg(authentication, workspaceId, begin, end, workType.id!!, offenceType.id!!)
                else
                    restTemplate.getCountByOffenceTypeNullGroupByOrg(authentication, workspaceId, begin, end, workType.id!!)

                byOrg.forEach {
                    // Заполняем найденными значениями карту по организациям.
                    // Берем id организации
                    val orgId               = it[0]?.toString() ?: "null"
                    // Заполняем значение по организации
                    val count               = mutableMapOf<String,Int>()
                    count["all"]            = it[1, BigInteger::class.java].toInt()
                    // Учитываем в общем числе по классификации
                    countByOffenceType["all"] = countByOffenceType["all"] as Int + it[1, BigInteger::class.java].toInt()
                    // Учитываем в общем числе по группе нарушений
                    countByWorkType["all"]  = countByWorkType["all"] as Int + it[1, BigInteger::class.java].toInt()
                    // Учитываем в карте по организация для группы нарушений
                    if (countsByOrgForWorkType.containsKey(orgId))
                        countsByOrgForWorkType[orgId]!!["all"] = countsByOrgForWorkType[orgId]!!["all"] as Int + it[1, BigInteger::class.java].toInt()
                    else
                        countsByOrgForWorkType[orgId]  = count
                    // Добавляем в карту по организациям для класса нарушений
                    countsByOrgForOffenceType[orgId]  = count

                    // Для каждой найденной организации проверяем, чтобы она была в общем списке.
                    if (!organizations.any { org -> org.id == if (orgId == "null") null else UUID.fromString(orgId) })
                    {
                        // Обрабатываем организацию с "null
                        if (orgId == "null")
                            organizations.add(0, COrganization())
                        else
                        {
                            // Ищем организацию по id в БД и добавляем в список
                            val orgOpt        = restTemplate.getOrganizationById(authentication, UUID.fromString(orgId))
                           // val orgOpt      = repositoryOrganization.findById(UUID.fromString(orgId))
                            if (orgOpt.isPresent)
                                organizations.add(orgOpt.get())
                        }
                    }
                }
                // Если для пустого типа нет организаций, то не добавляем его
                if (offenceType.id == null && countsByOrgForOffenceType.isEmpty())
                    return@offenceType

                countByOffenceType["countsByOrganization"] = countsByOrgForOffenceType
                // Добавляем запись во временный массив класса нарушений
                offenceTypesTemp.add(countByOffenceType)
            }

            countByWorkType["countsByOrganization"] = countsByOrgForWorkType
            // Добавляем в список данных по группе
            countsByType.add(countByWorkType)
            // Добавляем записи, которые мы создали ранее по классам у данной группы
            countsByType.addAll(offenceTypesTemp)
        }
        // Нужно добавить ещё одну строку по записям, у которых вообще не установлена группа нарушений
        // Записываем основные значения по группе
        val countByWorkType             = mutableMapOf<String, Any>()
        countByWorkType["type"]         = CWorkType()
        countByWorkType["isWorkType"]   = true
        countByWorkType["all"]          = 0

        val countsByOrgForWorkType      = mutableMapOf<String, MutableMap<String, Int>>()

        // Делаем запрос по организациям, где не установлена группа нарушений

        restTemplate.getCountByWorkTypeNullGroupByOrg(authentication, workspaceId, begin, end)
                .forEach {
                    // Заполняем найденными значениями карту по организациям.
                    // Берем id организации
                    val orgId           = it[0]?.toString() ?: "null"
                    // Заполняем значение по организации
                    val count           = mutableMapOf<String,Int>()
                    count["all"]        = it[1, BigInteger::class.java].toInt()
                    // Учитываем в общем числе по группе нарушений
                    countByWorkType["all"] = countByWorkType["all"] as Int + it[1, BigInteger::class.java].toInt()
                    // Добавляем в карту по организациям для группы нарушений
                    countsByOrgForWorkType[orgId]  = count

                    // Для каждой найденной организации проверяем, чтобы она была в общем списке.
                    if (!organizations.any { org -> org.id == if (orgId == "null") null else UUID.fromString(orgId) })
                    {
                        // Обрабатываем организацию с "null
                        if (orgId == "null")
                            organizations.add(0, COrganization())
                        else
                        {
                            // Ищем организацию по id в БД и добавляем в список
                            val orgOpt        = restTemplate.getOrganizationById(authentication, UUID.fromString(orgId))
                            if (orgOpt.isPresent)
                                organizations.add(orgOpt.get())
                        }
                    }
                }

        if (countsByOrgForWorkType.isNotEmpty())
        {
            countByWorkType["countsByOrganization"] = countsByOrgForWorkType
            // Добавляем в список данных по группе
            countsByType.add(countByWorkType)
        }

        // Заполняем результирующую карту
        result["organizations"]           = organizations
        result["countsByType"]            = countsByType
        return result
    }









    override fun getOrganizations(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            tableId                         : UUID?
    )                                       : List<COrganization>
    {
        return restTemplate.getOrganization(authentication, workspaceId, tableId).toList()
    }

    override fun get(
            authentication                  : OAuth2Authentication,
            workspaceId: UUID
    ) : String
    {
        return restTemplate.get(authentication, workspaceId)
    }
}