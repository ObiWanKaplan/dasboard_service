package ru.asstra.servicedashboard.services

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.HorizontalAlignment
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.VerticalAlignment
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.apache.tomcat.util.http.fileupload.ByteArrayOutputStream
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import ru.asstra.servicedashboard.model.COrganization
import java.net.URLEncoder
import java.util.*

@Service
class CServiceExcel                         : IServiceExcel
{


    override fun createExcel(
            organizations                   : List<COrganization>
    )                                       : XSSFWorkbook
    {
        val wb = XSSFWorkbook()
        val sheet = wb.createSheet("Sheet1") as XSSFSheet

        val row = arrayOfNulls<Row>(9)
        val cell = arrayOfNulls<Cell>(7)

        val style = wb.createCellStyle()
        style.shrinkToFit = true
        style.alignment = HorizontalAlignment.CENTER
        style.verticalAlignment = VerticalAlignment.CENTER

        sheet.getRow(0).getCell(0).setCellValue(organizations[0].name)


        //Формируем файл.
        //val wb                            = serviceExcelTaskList.getExcel(workspaceId, tableId, locale, filter, authentication)
        //Файл переводим в массив байт.
        val bos = ByteArrayOutputStream()
        bos.use { wb.write(it) }
        val bytes = bos.toByteArray()
        //Готовим ответ браузеру.
        val filename                        = URLEncoder.encode("Dashboard.xlsx", "UTF-8")
        val headers = HttpHeaders()
        headers.set("Content-Disposition", "attachment; filename*=UTF-8''$filename")
        headers.set("Access-Control-Expose-Headers", "Content-Disposition")
        headers.set("Content-Type", "application/vnd.ms-excel;")
        headers.set("content-length", Integer.toString(bytes.size))
        headers.set("content-length", bytes.size.toString())
//        when(browser)
//        {
//            "Firefox" -> headers.set("Content-Disposition", "attachment; filename*=UTF-8''$filename")
//            //Для всех остальных нет отличий.
//            //         "IE", "Chrome" -> headers.set("Content-Disposition", "attachment; filename=$filename")
//            else -> headers.set("Content-Disposition", "attachment; filename=$filename")
//        }
//          logger.info("Excel created.")

//        headers.set("Content-Disposition", "attachment; filename=dashboard.xlsx")
        return wb
    }
}