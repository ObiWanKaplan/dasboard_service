package ru.asstra.servicedashboard.services

import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.springframework.http.HttpEntity
import ru.asstra.servicedashboard.model.COrganization

interface IServiceExcel {

    fun createExcel(
            organizations                   : List<COrganization>
    )                                       : XSSFWorkbook
}