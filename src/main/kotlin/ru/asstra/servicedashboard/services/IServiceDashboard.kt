package ru.asstra.servicedashboard.services

import org.springframework.security.oauth2.provider.OAuth2Authentication
import ru.asstra.servicedashboard.model.COrganization
import ru.asstra.servicedashboard.model.CUser
import java.math.BigInteger
import java.time.LocalDate
import java.util.*

/********************************************************************************************************
 * Интерфейс содержит заголовки методов формирования данных для дашборда.                               *
 * @author Теплюк О.В. 2019 0726.                                                                       *
 *******************************************************************************************************/
interface IServiceDashboard
{
    fun getDocumentsCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID)
                                            : Long

    fun getDocumentsCountForPeriod(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID)
                                            : List<Map<String, Any>>

    fun documentsTotalSize(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID)
                                            : Long?

    fun getFilesCountByUser(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID)
                                            : Map<String, List<Pair<CUser?, Int>>>

    fun getUsersCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID)
                                            : Int

    fun getUserCountByOrg(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID)
                                            : List<Pair<COrganization, Int>>

    fun getActiveUserCountByOrganization(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID)
                                            : List<Pair<COrganization?, Int>>


//    fun getOrganizations(
//            authentication                  : OAuth2Authentication,
//            workspaceId                     : UUID,
//            tableId                         : UUID
//    )                                       : List<COrganization>

    fun getCountTicketsByOrgAndStatus(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            tableId                         : UUID)
                                            : Map<String, List<Any>>

    fun getTasksCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID)
                                            : Int

    fun getOffencesCount(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID)
                                            : Int

    fun getCountOffencesByWorkType(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID)
                                            : List<Map<String, Any>>

    fun getCountOffencesByOffenceType(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate)
                                            : List<Map<String, Any>>

    fun getCountOffencesByOrgAndTypes(
            authentication                  : OAuth2Authentication,
            dateFrom                        : LocalDate,
            dateTo                          : LocalDate,
            workspaceId                     : UUID,
            tableId                         : UUID?)
                                            : Map<String, List<Any>>

    fun getOrganizations(
            authentication                  : OAuth2Authentication,
            workspaceId                     : UUID,
            tableId                         : UUID?)
                                            : List<COrganization>

    fun get(
            authentication                  : OAuth2Authentication,
            workspaceId: UUID
    )   :   String

}