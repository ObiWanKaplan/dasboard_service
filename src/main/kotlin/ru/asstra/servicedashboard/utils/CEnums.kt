package ru.asstra.servicedashboard.utils

import java.util.*

/********************************************************************************************************
 *                                                                                                      *
 * Огурецкая Анна,  27.12.2018.                                                                         *
 ********************************************************************************************************/
enum class EventType(val id                 : UUID)
{
    CREATE(UUID.fromString("4beb2c8d-819e-409a-988c-f91368cf1ec2")),
    UPDATE(UUID.fromString("4682b09c-90d2-416e-961b-02d46a6e1ab6")),
    DELETE(UUID.fromString("a0619671-2b81-44fe-9f9f-c1b368f8ce2a")),
    DEACTIVATE(UUID.fromString("86672505-31b8-4304-adf2-192fe0b97572")),
    USER_COMMENT(UUID.fromString("7a8c3615-f6af-4ee3-8e7a-4140321d9a7c")),
    COPY(UUID.fromString("dbf090ae-32c4-4ad3-969f-31e849a9cf1e")),
    MOVE(UUID.fromString("6044c334-5798-42ef-9085-89a9f1816198"))
}

enum class ItemType (
        val `class`                         : String,
        val accessRight                     : Pair<String, Int?>,
        val eventTypeList                   : List<EventType>)
{
    FILE(                   "CFile",                        Pair("FILES", null),                    listOf(EventType.CREATE, EventType.DEACTIVATE, EventType.MOVE, EventType.COPY) ),
    TASK(                   "CTask",                        Pair("TASKS", null),                    listOf(EventType.CREATE, EventType.UPDATE, EventType.DEACTIVATE, EventType.USER_COMMENT) ),
    USER_FOLDER_RELATION(   "CUserFolderRelation",          Pair("EDIT_FOLDER_USER_ACCESS", null),  listOf(EventType.UPDATE) ),
    USER_WORKSPACE_RELATION("CUserWorkspaceRelation",       Pair("CONTACTS", 2),                    listOf(EventType.UPDATE) ),
    ROLE_USER(              "CRoleUser",                    Pair("CONTACTS", 2),                    listOf(EventType.UPDATE) ),
    OBJECT(                 "CObject",                      Pair("OBJECTS", null),                  listOf(EventType.CREATE, EventType.DEACTIVATE) ),
    ROLE(                   "CRole",                        Pair("ROLES", null),                    listOf(EventType.CREATE, EventType.DEACTIVATE) ),
    USER(                   "CUser",                        Pair("ROLES", null),                    listOf(EventType.CREATE) ),
    WORKSPACE(              "CWorkspace",                   Pair("WORKSPACES", null),               listOf(EventType.CREATE, EventType.DEACTIVATE)),
    ACCESS_RIGHT_IN_ROLES(  "CAccessRightRoleRelation",     Pair("ROLES", null),                    listOf(EventType.UPDATE)),
    EMPTY_CLASS(            "EmptyClass",                   Pair("CHAT", null),                     listOf(EventType.USER_COMMENT)),
    NEWS_FILTER (           "CNewsFilter",                  Pair("ADMIN", null),                    listOf(EventType.CREATE, EventType.UPDATE, EventType.DEACTIVATE) ),
    USER_NEWS_FILTER(       "CUserNewsFilter",              Pair("ADMIN", null),                    listOf(EventType.UPDATE)),
    WORK_TYPE(              "CWorkType",                    Pair("WORK_TYPES", null),               listOf(EventType.CREATE, EventType.DEACTIVATE)),
    OFFENCE_TYPE(           "COffenceType",                 Pair("OFFENCE_TYPES", null),            listOf(EventType.CREATE, EventType.DEACTIVATE)),
    WORK_TYPE_OFFENCE_TYPE( "CWorkTypeOffenceTypeRelation", Pair("WORK_TYPES", 2),                  listOf(EventType.UPDATE)),
    OFFENCE(                "COffence",                     Pair("OFFENCES", null),                 listOf(EventType.CREATE, EventType.UPDATE, EventType.DEACTIVATE, EventType.USER_COMMENT)),
    ORDER (                 "COrder",                       Pair("ORDERS", null),                   listOf(EventType.CREATE, EventType.UPDATE, EventType.DEACTIVATE))
}