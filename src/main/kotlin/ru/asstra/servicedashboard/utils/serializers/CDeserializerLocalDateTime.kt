package ru.swiftgroup.project.api.utils.serializers

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class CDeserializerLocalDateTime             : JsonDeserializer<LocalDateTime>()
{
    override fun deserialize(
            p                               : JsonParser?,
            ctxt                            : DeserializationContext?)
                                            : LocalDateTime
    {
        val formatter                       = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")
        return LocalDateTime.parse(p!!.readValueAs(String::class.java), formatter)
    }

}