package ru.asstra.servicedashboard.model

import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.ArrayList

/*******************************************************************************************************
 * Класс описывает права доступа пользовтелей.                                                         *
 * @author Теплюк О.В.  2018 0904.                                                                     *
 *******************************************************************************************************/

data class CAccessRight
(
    /****************************************************************************************************
     * Код прав доступа.                                                                                *
     ****************************************************************************************************/
    var code                                : String?
                                            = null,

    @JsonIdentityReference(alwaysAsId       = true)
    @JsonIgnore
    var accessRightsInRoles                 : MutableList<CAccessRightRoleRelation>
                                            = ArrayList()

)                                           : CNamedObject()
{
    override fun toString()                 : String
    {
        return "name: " + name
    }

}