package ru.asstra.servicedashboard.model


/********************************************************************************************************
 * Справочник типов событий                                                                             *
 * @author Огурецкая Анна,  27.12.2018.                                                                 *
 *******************************************************************************************************/
data class CEventType
(

        var code                            : String?
                                            = null

)                                           : CNamedObject()