package ru.asstra.servicedashboard.model.tickets

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import ru.asstra.servicedashboard.model.CAbstractObject
import ru.asstra.servicedashboard.model.CFile

data class CTicketAttachmentRelation
(
        /****************************************************************************************************
         * Ссылка на поручение                                                                              *
         ***************************************************************************************************/
        @JsonIgnore
        var ticket                              : CTicket?
                                                = null,

        /****************************************************************************************************
         * Ссылка на поручение                                                                              *
         ***************************************************************************************************/
        var file                                : CFile?
                                                = null

)                                               : CAbstractObject()