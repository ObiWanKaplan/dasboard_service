package ru.asstra.servicedashboard.model.tickets.statuses

import ru.asstra.servicedashboard.model.CNamedObject
import ru.asstra.servicedashboard.model.CWorkspace
import java.util.*

data class CStatus
(
    /****************************************************************************************************
     * Начальное состояние системы статусов                                                             *
     * Если true, то устанавливаем этот статус как начальный при создании                               *
     ***************************************************************************************************/
    var isStart                             : Boolean?
                                            = false,

    /****************************************************************************************************
     * Код статуса                                                                                      *
     ***************************************************************************************************/
    var code                                : String?
                                            = null,

    /****************************************************************************************************
     * Цвет раскраски статуса                                                                           *
     ***************************************************************************************************/
    var color                               : String?
                                            = null,

    /****************************************************************************************************
     * Принадлежность к рабочей области.                                                                *
     * Если null, значит используем по умолчанию                                                        *
     ***************************************************************************************************/
    var workspace                           : CWorkspace?
                                            = null,

    /****************************************************************************************************
     * Идентификатор таблицы, в которой должен исользоваться данный статус                              *
     ***************************************************************************************************/
    var tableId                             : UUID?
                                            = null

)                                           : CNamedObject()