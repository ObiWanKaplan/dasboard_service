package ru.asstra.servicedashboard.model.tickets

import com.fasterxml.jackson.annotation.JsonFormat
import ru.asstra.servicedashboard.model.*
import ru.asstra.servicedashboard.model.tickets.statuses.CStatus
import ru.swiftgroup.project.api.model.events.CEvent
import java.time.LocalDateTime
import java.util.ArrayList

/*******************************************************************************************************
 * Базовый класс для поручений, нарушений, инспекций.                                                  *
 * @author Огрурецкая А.И. 2019 0507                                                                   *
 *******************************************************************************************************/
open class CTicket                          : CNamedObject()
{
    /****************************************************************************************************
     * Порядковый номер задания                                                                         *
     ***************************************************************************************************/
    open var sequenceNumber                 : Long?
                                            = null

    /****************************************************************************************************
     * Ссылка на объект                                                                                 *
     ***************************************************************************************************/
    open var `object`                       : CObject?
                                            = null

    /****************************************************************************************************
     * Создатель                                                                                        *
     ***************************************************************************************************/
    open var creator                        : CUser
                                            = CUser()

    /****************************************************************************************************
     * Инициатор                                                                                        *
     ***************************************************************************************************/
    open var initiator                      : CUser?
                                            = null

    /****************************************************************************************************
     * Организация исполнителя.                                                                         *
     ***************************************************************************************************/
    open var organization                   : COrganization?
                                            = null

    /****************************************************************************************************
     * Исполнитель                                                                                      *
     ***************************************************************************************************/
    open var assignee                       : CUser?
                                            = null

    /****************************************************************************************************
     * Срок исполнения задания.                                                                         *
     ***************************************************************************************************/
    @JsonFormat(pattern                     = "yyyy-MM-dd HH:mm:ss.SSS")
    open var deadline                       : LocalDateTime?
                                            = null

    /****************************************************************************************************
     * Дата фактического исполнения задания.                                                            *
     ***************************************************************************************************/
    @JsonFormat(pattern                     = "yyyy-MM-dd HH:mm:ss.SSS")
    open var endDate                        : LocalDateTime?
                                            = null

    /****************************************************************************************************
     * Статус                                                                                           *
     ***************************************************************************************************/
    open var status                         : CStatus?
                                            = null

    /****************************************************************************************************
     * Уровень риска                                                                                    *
     ***************************************************************************************************/
    open var riskLevel                      : CRiskLevel?
                                            = null

    /****************************************************************************************************
     * Ссылки на вложения                                                                               *
     ***************************************************************************************************/
    open var attachment                     : MutableList<CTicketAttachmentRelation>
                                            = ArrayList()

    /****************************************************************************************************
     * Комментарии                                                                                      *
     ***************************************************************************************************/
    @Transient
    var comments                            : Iterable<CEvent>?
                                            = null

}