package ru.asstra.servicedashboard.model

import com.fasterxml.jackson.annotation.JsonIdentityReference
import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.ArrayList

/*******************************************************************************************************
 * Класс описывает роли пользователей                                                                  *
 * @author Теплюк О.В.  2018 0904.                                                                     *
 *******************************************************************************************************/
data class CRole
(
    @JsonIdentityReference(alwaysAsId       = true)
    @JsonIgnore
    var rolesUsers                          : MutableList<CRoleUser>
                                            = ArrayList(),

    @JsonIdentityReference(alwaysAsId       = true)
    @JsonIgnore
    var accessRightsInRoles                 : MutableList<CAccessRightRoleRelation>  = ArrayList()

)                                           : CNamedObject()
{
    override fun toString()                 : String
    {
        return "name: $name"
    }
}