package ru.asstra.servicedashboard.model

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.annotation.JsonSerialize
import ru.swiftgroup.project.api.utils.serializers.CDeserializerLocalDateTime
import ru.swiftgroup.project.api.utils.serializers.CSerializerLocalDateTime
import java.util.*
import java.time.LocalDateTime

/********************************************************************************************************
 * Базовый класс для всех объектов модели.                                                              *
 * @author Селетков И.П. 2018 0609.                                                                     *
 *******************************************************************************************************/
abstract class CAbstractObject
{
    /****************************************************************************************************
     * Идентификатор.                                                                                   *
     ***************************************************************************************************/

    @JsonProperty("id")
    var id                                  : UUID?
                                            = null

    /****************************************************************************************************
     * Дата и время создания.                                                                           *
     ***************************************************************************************************/
    @JsonSerialize(using                    = CSerializerLocalDateTime::class)
    @JsonDeserialize(using                  = CDeserializerLocalDateTime::class)
    open var createdAt                      : LocalDateTime?
                                            = null

    /****************************************************************************************************
     * Дата и время обновления.                                                                         *
     ***************************************************************************************************/
    @JsonSerialize(using                    = CSerializerLocalDateTime::class)
    @JsonDeserialize(using                  = CDeserializerLocalDateTime::class)
    open var updatedAt                      : LocalDateTime?
                                            = null

    /****************************************************************************************************
     * Активность объекта.                                                                              *
     ***************************************************************************************************/
    open var active                         : Boolean
                                            = false

}