package ru.asstra.servicedashboard.dto

import java.util.*

class CDTOIDCount (
        var id : UUID,
        var count : Long
)