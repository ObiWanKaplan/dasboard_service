package ru.asstra.servicedashboard.config

import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.MessageSource
import org.springframework.context.support.ResourceBundleMessageSource
import org.springframework.core.annotation.Order
import org.springframework.http.HttpMethod
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter

@Configuration
@Order(0)
@EnableResourceServer
open class CResourceServerConfiguration     : ResourceServerConfigurerAdapter()
{
    @Throws(Exception::class)
    override fun configure(http: HttpSecurity)
    {
        http.authorizeRequests()
            .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
            .antMatchers("/swagger-ui.html").permitAll()
            .antMatchers("/").permitAll()
            .antMatchers("/dashboard/*").authenticated()
            //.anyRequest().authenticated()
            //.anyRequest().permitAll()
    }

    /****************************************************************************************************
     * Возвращает объект с переводами текстовых констант.                                               *
     ***************************************************************************************************/
    @Bean
    open fun messageSource()                : MessageSource
    {
        val messageSource                   = ResourceBundleMessageSource()
        messageSource.setBasename("i18n/messages")
        messageSource.setDefaultEncoding("UTF-8")
        return messageSource
    }

}