package ru.asstra.servicedashboard.config

import org.springframework.boot.web.servlet.FilterRegistrationBean
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.Ordered
import org.springframework.web.cors.CorsConfiguration
import org.springframework.web.cors.UrlBasedCorsConfigurationSource
import org.springframework.web.filter.CorsFilter

//@Configuration
open class CCORSConfig
{
    @Bean
    open fun simpleCorsFilter()             : FilterRegistrationBean<CorsFilter>
    {
        val source                          = UrlBasedCorsConfigurationSource()
        val config                          = CorsConfiguration()
        config.allowCredentials             = true
        config.allowedOrigins               = listOf("*")
        config.allowedMethods               = listOf("GET", "POST", "PATCH", "PUT", "DELETE")
        config.allowedHeaders               = listOf("*")
        source.registerCorsConfiguration("/**", config)

        val bean                            = FilterRegistrationBean(CorsFilter(source))
        bean.order                          = Ordered.HIGHEST_PRECEDENCE
        return bean
    }

}